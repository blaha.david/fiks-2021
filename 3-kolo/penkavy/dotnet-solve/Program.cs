﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

// Ideas:
// compare strings linearly and start levenshtein from the first difference
// the same from the end
// put the shorter one on the top
// scan rows


// Single thread: vstup 0.-7. = 20s

public struct Zadani
{
    public int N;
    public int K;
    public char[][] DNAs;
    public int indexZadani;

    public Zadani(int n, int k, char[][] dNAs, int indexZadani)
    {
        N = n;
        K = k;
        DNAs = dNAs;
        this.indexZadani = indexZadani;
    }
}

public class Program
{
    public static char[] NUKLEOTIDE = new char[] { 'A', 'C', 'G', 'T' };

    public static void Main(string[] args)
    {

        using (BinaryReader reader = new BinaryReader(new FileStream(args[0], FileMode.Open)))
        {
            int T = int.Parse(new string(readUntil(reader, '\n')));

            Dictionary<(int, int), bool>[] results = new Dictionary<(int, int), bool>[T];
            Zadani[] zadanis = new Zadani[T];
            Thread[] threads = new Thread[T];

            for (int t = 0; t < T; t++)
            {
                Zadani z = SolveZadani(reader, t);

                zadanis[t] = z;

                int copyT = t;
                Thread thread = new Thread(() =>
                {
                    results[copyT] = SolveLevenshtein(z);
                });
                thread.Start();
                threads[t] = thread;
                //thread.Join();
            }
            for (int t = 0; t < T; t++)
            {
                threads[t].Join();
            }

            //Console.Error.WriteLine("Solving trojice");

            for (int t = 0; t < T; t++)
            {
                Zadani z = zadanis[t];
                List<(int, int, int)> trojice = SolveTrojice(results[t], z.N);

                // Output result:
                Console.WriteLine(trojice.Count);
                foreach (var troj in trojice)
                {
                    Console.WriteLine("{0} {1} {2}", troj.Item1, troj.Item2, troj.Item3);
                }
            }
        }
    }

    public static Zadani SolveZadani(BinaryReader reader, int indexZadani)
    {
        //Console.WriteLine($"\nZadani: peek N=0x{reader.PeekChar():x}");

        int N = int.Parse(new string(readUntil(reader, ' ')));
        int K = int.Parse(new string(readUntil(reader, '\n')));

        //Console.WriteLine($"N={N}m K={K}");

        byte[][] rawDNAs = new byte[N][];
        char[][] DNAs = new char[N][];

        for (int n = 0; n < N; n++)
        {
            string lenStr = new string(readUntil(reader, ' '));

            int len = int.Parse(lenStr);

            int lenToRead = (int)Math.Ceiling(len / 4.0m);

            rawDNAs[n] = reader.ReadBytes(lenToRead);

            //Console.WriteLine($"Peek after read binary: 0x{reader.PeekChar():x}");
            byte skip = (byte)reader.Read(); // Skip new line character at the end

            //Console.WriteLine($"Skipped 0x{skip:x}");

            DNAs[n] = new char[len];

            int index = 0;
            for (int i = 0; i < lenToRead; i++)
            {
                byte b = rawDNAs[n][i];
                DNAs[n][index++] = NUKLEOTIDE[b >> 6 & 0b11];
                if (index < len)
                    DNAs[n][index++] = NUKLEOTIDE[b >> 4 & 0b11];
                if (index < len)
                    DNAs[n][index++] = NUKLEOTIDE[b >> 2 & 0b11];
                if (index < len)
                    DNAs[n][index++] = NUKLEOTIDE[b & 0b11];
            }

            //Console.WriteLine(string.Join(',', DNAs[n]));
        }

        return new Zadani(N, K, DNAs, indexZadani);

    }

    public static Dictionary<(int, int), bool> SolveLevenshtein(Zadani z)
    {
        Dictionary<(int, int), bool> podobne = new Dictionary<(int, int), bool>();

        //Console.WriteLine("Calculating Levenshtein distances...");
        for (int a = 0; a < z.N; a++)
        {
            for (int b = a + 1; b < z.N; b++)
            {
                podobne.Add((a, b), Levenshtein(z.DNAs[a], z.DNAs[b], z.K));
            }
        }
        return podobne;
    }

    public static List<(int, int, int)> SolveTrojice(Dictionary<(int, int), bool> podobne, int N)
    {
        List<(int, int, int)> trojice = new List<(int, int, int)>();

        //Console.WriteLine("Trying combinations...");

        for (int a = 0; a < N; a++)
        {
            for (int b = 0; b < N; b++)
            {
                if (a != b)
                {
                    if (Lookup(podobne, a, b))
                    {
                        for (int c = a + 1; c < N; c++)
                        {
                            if (a != c && b != c)
                            {
                                if (!Lookup(podobne, a, c))
                                {
                                    if (Lookup(podobne, b, c))
                                    {
                                        trojice.Add((a, b, c));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return trojice;
    }

    public static bool Lookup(Dictionary<(int, int), bool> podobne, int a, int b)
    {
        if (podobne.ContainsKey((a, b)))
        {
            return podobne[(a, b)];
        }
        else if (podobne.ContainsKey((b, a)))
        {
            return podobne[(b, a)];
        }

        return false; // shouldn't get here
    }

    //public static char[] readUntil(StreamReader sr, char end)
    //{
    //List<char> bytes = new List<char>();

    //char ch = (char)sr.Read();
    //while (ch != end)
    //{
    //bytes.Add(ch);
    //ch = (char)sr.Read();
    //}

    //return bytes.ToArray();
    //}

    public static char[] readUntil(BinaryReader reader, char end)
    {
        //Console.WriteLine($"\nreadUntil(reader,0x{((byte)end):x}='{end}')");
        List<char> bytes = new List<char>();

        char ch = (char)reader.Read();
        while (ch != end)
        {
            //Console.WriteLine($"Reading 0x{((byte)ch):x}");
            bytes.Add(ch);
            ch = (char)reader.Read();
        }

        return bytes.ToArray();
    }

    public static bool Levenshtein(char[] a, char[] b, int limit)
    {

        int diffLen = a.Length - b.Length;

        if (diffLen > limit || diffLen < -limit) return false;

        char[] top = a.Length < b.Length ? a : b;
        char[] left = a.Length < b.Length ? b : a;

        int topLen = top.Length + 1;

        int[] prevRow = new int[topLen];
        int[] curRow = new int[topLen];

        for (int i = 0; i < topLen; i++)
        {
            prevRow[i] = i;
        }

        for (int j = 1; j <= left.Length; j++)
        {

            int minValueInRow = int.MaxValue; // optimalizace

            for (int i = 0; i < topLen; i++)
            {
                if (i == 0)
                {
                    curRow[i] = j;
                    continue;
                }

                int cost = top[i - 1] == left[j - 1] ? 0 : 1;

                int l = curRow[i - 1] + 1; // left
                int t = prevRow[i] + 1; // top
                int d = prevRow[i - 1] + cost; // diagonal

                int n = d < l ? (d < t ? d : t) : (t < l ? t : l); // min(l,t,d)

                curRow[i] = n;

                if (n < minValueInRow) minValueInRow = n; // optimalizace
            }

            if (minValueInRow > limit) return false;

            //Console.WriteLine(String.Join(',', curRow));

            // Swap buffers:
            (prevRow, curRow) = (curRow, prevRow);
        }

        return prevRow[topLen - 1] <= limit;
    }
}
