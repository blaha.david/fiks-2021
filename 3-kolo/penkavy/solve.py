#! /usr/bin/env python3

import sys
import math
import textwrap
from pprint import pprint


NUKLEOTIDE = {
    '00': 'A',
    '01': 'C',
    '10': 'G',
    '11': 'T'
}

def readuntil(file, end):
    buffer = b''

    ch = file.read(1)
    while ch != end:
        buffer += ch
        ch = file.read(1)
    return buffer

def solve(dnas: list[str], K: int):
    pprint(dnas)
    pass



def zadani(file):
    N, K = [ int(x) for x in file.readline().decode('utf-8').split(' ') ]

    dnas = []

    for _ in range(N):
        size = int(readuntil(file, b' '))
        raw_dna = file.read(math.ceil(size/4))
        dna_bits = textwrap.wrap(''.join([ bin(ch)[2:].rjust(8, '0') for ch in raw_dna ]),2)
        dna = [ NUKLEOTIDE[dna_bit_pair] for dna_bit_pair in dna_bits ][:size]

        dnas.append(dna)

        # oneliner
        print(''.join([ NUKLEOTIDE[dna_bit_pair] for dna_bit_pair in textwrap.wrap(''.join([ bin(ord(ch))[2:].rjust(8, '0') for ch in line[1] ]),2) ][:size]))

    solve(dnas, K)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Required exactly one argument")
        sys.exit(1)

    with open(sys.argv[1], 'rb') as file:
        T = int(file.readline())
        for t in range(T):
            zadani(file)
