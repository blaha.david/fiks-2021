
String top = "nejakejstring";
String left = "jinejstring";

int[][] matrix;
int matrixRows;
int matrixCols;

int currentRow = 1;

int TEXTSIZE = 20;

void setup () {
  size(600, 600);

  noLoop();
  textAlign(CENTER);
  textSize(TEXTSIZE);

  //construct matrix
  matrixRows = left.length()+1;
  matrixCols = top.length()+1;
  matrix = new int[matrixRows][];
  for (int j = 0; j < matrixRows; j++) {
    matrix[j] = new int[matrixCols];
    for (int i = 0; i < matrixCols; i++) {
      if (j == 0) {
        matrix[j][i] = i;
      }
      if (i == 0) {
        matrix[j][i] = j;
      }
    }
  }
}

void solveRow () {
  for (int i = 1; i < matrixCols; i++) {
    int[] row = matrix[currentRow];
    int cost = top.charAt(i-1) == left.charAt(currentRow-1) ? 0 : 1;
    int l = row[i-1]+1; // left
    int t = matrix[currentRow-1][i]+1; // top
    int d = matrix[currentRow-1][i-1] + cost; // diagonal

    int n = d < l ? (d < t ? d : t) : (t < l ? t : l); // min(l,t,d)
    
    row[i] = n;
  }
  currentRow++;
}

void draw () {
  int cols = top.length()+2;
  int rows = left.length()+2;

  float w = width/cols;
  float h = height/rows;



  for (int j = 0; j < rows; j++) {
    for (int i = 0; i < cols; i++) {
      stroke(255);
      fill(j-1 == currentRow ? 80 : 50);
      rect(i*w, j*h, w, h);

      if (j == 0 && i >= 2) {
        fill(255);
        text(top.charAt(i-2), i*w+(w/2), TEXTSIZE);
      }
      if (i == 0 && j >= 2) {
        fill(255);
        text(left.charAt(j-2), (w/2), j*h+TEXTSIZE);
      }
      if (i >= 1 && j >= 1) {
        fill(250, 150, 0);
        text(matrix[j-1][i-1], i*w+(w/2), j*h+TEXTSIZE);
      }
    }
  }
}

void keyPressed () {
  solveRow();
  redraw();
}
