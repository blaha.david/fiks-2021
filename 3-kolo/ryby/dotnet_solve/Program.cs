﻿using System;

class Program
{
    struct Vertex
    {
        public long x;
        public long y;
        public long dist;
        public bool start;

        public Vertex(long x, long y, long dist)
        {
            this.x = x;
            this.y = y;
            this.dist = dist;
            this.start = false;
        }
        public void SetStart()
        {
            start = true;
        }
    }

    public static void Main()
    {
        string[] line = Console.ReadLine().Split(' ');
        long N = long.Parse(line[0]);
        long sx = long.Parse(line[1]);
        long sy = long.Parse(line[2]);

        long a = -sy; // Normal
        long b = sx;

        Vertex[] vertexes = new Vertex[N*2];
        long index = 0;

        for (long n = 0; n < N; n++)
        {
            string[] polygon = Console.ReadLine().Split(' ');
            long p = long.Parse(polygon[0]);

            long minDist = long.MaxValue;
            long minIndex = 0;
            long maxDist = long.MinValue;
            long maxIndex = 0;

            Vertex[] polygonVertexes = new Vertex[p];

            for (long i = 0; i < p; i++)
            {
                long x = long.Parse(polygon[i * 2 + 1]);
                long y = long.Parse(polygon[i * 2 + 2]);
                long dist = a*x + b*y;

                if (dist < minDist)
                {
                    minDist = dist;
                    minIndex = i;
                }
                if (dist > maxDist)
                {
                    maxDist = dist;
                    maxIndex = i;
                }

                polygonVertexes[i] = new Vertex(x, y, dist);
            }

            polygonVertexes[minIndex].SetStart();
            vertexes[index++] = polygonVertexes[minIndex];
            vertexes[index++] = polygonVertexes[maxIndex];
        }



        Array.Sort(vertexes, (v1, v2) =>
        {
            if (v1.dist == v2.dist)
            {
                if (v1.start ^ v2.start)
                {
                    return v1.start ? -1 : 1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return v1.dist < v2.dist ? -1 : 1;
            }
        });



        long current = 0;
        long max = 0;
        foreach (var v in vertexes)
        {
            if (v.start)
            {
                current++;
                if (current > max) max = current;
            }
            else
            {
                current--;
            }
        }

        Console.WriteLine(max);
    }
}
