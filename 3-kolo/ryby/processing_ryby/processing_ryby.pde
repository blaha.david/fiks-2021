
int[][] polygons;

int sx;
int sy;

int SCALE = 50;

int currentPolygonIndex = 0;
int currentVertexIndex = 0;

void setup () {
  size(600, 600);


  //sx = 0;
  //sy = 1;
  //polygons = new int[3][];
  //polygons[0] = new int[]{0, 0, 0, 1, 3, 0};
  //polygons[1] = new int[]{0, 2, 1, 2, 1, 1};
  //polygons[2] = new int[]{2, 0, 2, 1, 3, 2, 4, 2, 4, 1};

  //sx = 1;
  //sy = 1;
  //polygons = new int[4][];
  //polygons[0] = new int[]{0, 0, 0, 1, 1, 1, 1, 0};
  //polygons[1] = new int[]{2, 0, 2, 1, 3, 1, 3, 0};
  //polygons[2] = new int[]{0, 2, 0, 3, 1, 3, 1, 2};
  //polygons[3] = new int[]{2, 2, 2, 3, 3, 3, 3, 2};

  sx = 3;
  sy = 4;
  polygons = new int[5][];
  polygons[0] = new int[]{1, 10, 4, 9, 0, 0, 8, 3, 8, 8};
  polygons[1] = new int[]{9, 3, 7, 5, 1, 5, 1, 9, 5, 0};
  polygons[2] = new int[]{4, 4, 8, 2, 10, 8, 3, 2};
  polygons[3] = new int[]{8, 6, 6, 9, 5, 10};
  polygons[4] = new int[]{0, 0, 6, 2, 8, 4, 2, 2, 7, 7};
  
  
  
  int a = -sy; // Normal
  int b = sx;
  
  println(distance(a,b,0,0,2));
  

  ellipseMode(CENTER);
}

void draw () {
  background(50);

  pushMatrix();

  translate(30, height-30);
  scale(1, -1);

  for (int i = 0; i < polygons.length; i++) {
    beginShape();


    for (int j = 0; j < polygons[i].length; j += 2) {

      strokeWeight(1);
      if (i == currentPolygonIndex && j == currentVertexIndex) {
        stroke(0, 255, 0);
        fill(0, 255, 0, 255);
      } else {
        stroke(255);
        fill(255, 255);
      }
      ellipse(polygons[i][j]*SCALE, polygons[i][j+1]*SCALE, 10, 10);

      //fill(255, 100);
      vertex(polygons[i][j]*SCALE, polygons[i][j+1]*SCALE);
    }

    stroke(255);
    fill(255, 100);
    endShape(CLOSE);
  }


  int p = currentPolygonIndex;
  int v = currentVertexIndex;
  //int offsetX = polygons[p][v];
  //int offsetY = polygons[p][v + 1];
  int offsetX = 0;
  int offsetY = 0;

  stroke(255, 0, 0);
  strokeWeight(2);
  int lx = (sx)*10+offsetX;
  int ly = (sy)*10+offsetY;
  line(offsetX*SCALE, offsetY*SCALE, lx*SCALE, ly*SCALE);


  popMatrix();
}

int solve(int sx, int sy, int[][] polygons) {
  int max = 0;
  for (int p = 0; p < polygons.length; p++) {
    for (int v = 0; v < polygons[p].length; v += 2) {
      int current = 1;

      for (int op = p + 1; op < polygons.length; op++) { // Other polygon
        for (int ov = 0; ov < polygons[op].length; ov += 2) { // Other vertex
          if (intersect(sx, sy, polygons[p][v], polygons[p][v + 1], polygons[op][ov], polygons[op][ov + 1])) {
            current++;
            break; // optimalizace
          }
        }
      }

      if (current > max) {
        max = current;
      }
    }
  }
  return max;
}

int step(int sx, int sy, int[][] polygons) {
  int current = 1;

  int p = currentPolygonIndex;
  int v = currentVertexIndex;

  for (int op = p + 1; op < polygons.length; op++) { // Other polygon
    for (int ov = 0; ov < polygons[op].length; ov += 2) { // Other vertex
      if (intersect(sx, sy, polygons[p][v], polygons[p][v + 1], polygons[op][ov], polygons[op][ov + 1])) {
        current++;
        break; // optimalizace
      }
    }
  }
  

  return current;
}

float distance(int a, int b, int c, int x, int y) {
  return (a*x + b*y + c) / sqrt(a*a + b*b);
}

boolean intersect(int sx, int sy, int ax, int ay, int bx, int by) {
  // ax + by + c = 0

  int a = -sy; // Normal
  int b = sx;

  int x1 = a * ax;
  int y1 = b * ay;

  int c1 = -(x1 + y1);


  int x2 = a * bx;
  int y2 = b * by;

  int c2 = -(x2 + y2);

  return c1 == c2;
}




void keyPressed () {
  println(step(sx, sy, polygons));
  currentVertexIndex += 2;
  if (currentVertexIndex >= polygons[currentPolygonIndex].length) {
    currentVertexIndex = 0;
    currentPolygonIndex++;
  }
}
