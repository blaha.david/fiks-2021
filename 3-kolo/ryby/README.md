
# Ryby


## Popis algoritmu

Algoritmus nejprve spočítá obecnou rovnici přímky podle dané směrnice,
kterou převede na normálový vektor přímky, a bodu `[0,0]` (Mohl by být jakýkoliv bod).
Vychází ze vzorce `V = (a*x + b*y + c) / sqrt(a^2 + b^2)` kde *a*, *b* a *c* vychází z obecné rovnice přímky a *x* a *y* jsou souřadnice bodu.
Jelikož bod je `[0,0]` => *c*=0, a *a* a *b* se nemění - díky tomu zůstává jmenovatel v případě všech vrcholů stejný - můžeme vzorec zjednodušit:
`V = a*x + b*y`.
Tím se zároveň eliminuje problém s čísly s desetinnou čístí (float, double) a jejich nepřesnostmi.


Poté načítá vrcholy polygonů, přičemž vždy spočítá jeho vzdálenost od této přímky
(vzdálenost není v absolutní hodnotě - vrcholy na jedné straně přímky budou mít kladnou vzdálenost, vrcholy na opačné straně přímky zápornou)
a pro každý polygon uchovává "začátek" a "konec" tohoto polygonu.
"Začátek" a "konec" jsou dva od sebe nejvzdálenější vrcholy ve směru kolmém na přímku.
("Začátek" má nejmenší hodnotu vypočítané vzdálenost, "konec" největší)

Po zpracování každého se tyto dva vrcholy přidají do pole vrcholů,
první z nich se označí jako začátek.

Všechny vrcholy ve vytvořeném poli se seřadí podle vzdálenosti od přímky.
Pokud mají shodnou vzdálenost, prioritu má ten, který je "začínající".
Díky tomu při následném procházení se nejdřív připočtou okraje následujících "začínajících" polygonů
a až poté se odečtou předchozí "končící" polygony.

Vytvoří proměné pro uchování aktuálnímu a maximálnímu počtu protnutých polygonů.

Postupně projde všechny polygonu v poli a pokud je polygon začínající, přičte polygon k aktuálnímu počtu protnutých polygonů, pokud ne, tak ho odečte.
Pokud je aktuální počet větší než maximální, nastaví maximální na aktuální.

Po tom, co projde všechny vrcholy vypíše uchované maximum.


## Asymptotická složitost

Algoritmus se skládá ze tří částí:
1. Počítání vzdálenosti vrcholů a určování začátku a konce každého polygonu. `O(n)`
2. Seřazení pole vrcholů. `O(nlog(n))`
3. Získání maxima protnutých polygonů. `O(n)`

Celkem `O(n + nlog(n) + n)`.

Tedy `O(nlog(n))` (nejpomalejší částí je řazení)


