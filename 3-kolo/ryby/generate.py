#! /usr/bin/env python3

import sys
import random

N = 5

def gcd(a, b):
    if b == 0:
        return a
    return gcd(b, a % b)


sx = random.randint(0,5)
sy = random.randint(0,5)
if sx == 0 and sy == 0:
    sy = 1

g = gcd(sx,sy)
sx //= g
sy //= g

print('{} {} {}'.format(N, sx, sy))


for _ in range(N):
    v_len = random.randint(3,random.randint(4,7))
    print(v_len, end=' ')
    for _ in range(v_len):
        x = random.randint(0,10)
        y = random.randint(0,10)
        print('{} {}'.format(x,y), end=' ')
    print()


