
# FIKS 2021

## 3. kolo

### [Pěnkavy](penkavy/) `10/10`
Odpověz Sfinze!
[download pdf](https://fiks.fit.cvut.cz/files/tasks/season8/round3/penkavy.pdf)


### [Sloni](sloni/) `10/10`
Odpověz Sfinze!
[download pdf](https://fiks.fit.cvut.cz/files/tasks/season8/round3/sloni.pdf)


### [Ryby](ryby/) `10/10`
Rozmysli, popiš a naprogramuj!
[download pdf](https://fiks.fit.cvut.cz/files/tasks/season8/round3/ryby.pdf)


