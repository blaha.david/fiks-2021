#include <stdio.h>

using ll = long long int;

int solve_grid(int W, int H, int X, int Y, int T) {
    bool** grid = new bool*[H];
    for (int j = 0;j < H;j++) {
        grid[j] = new bool[W];
    }
    bool** newgrid = new bool*[H];
    for (int j = 0;j < H;j++) {
        newgrid[j] = new bool[W];
    }

    grid[Y][X] = true;

    for (int t = 1; t <= T; t++) {
        for (int j = 0;j < H;j++) {
            for (int i = 0;i < W;i++) {
                newgrid[j][i] = false;
            }
        }
        for (int j = 0;j < H;j++) {
            for (int i = 0;i < W;i++) {
                if (grid[j][i]) {
                    if (i > 0)
                        newgrid[j][i-1] = true;
                    if (j > 0)
                        newgrid[j-1][i] = true;
                    if (i < W-1)
                        newgrid[j][i+1] = true;
                    if (j < H-1)
                        newgrid[j+1][i] = true;
                }
            }
        }

        // swap
        bool** temp = grid;
        grid = newgrid;
        newgrid = temp;
    }


    int count = 0;
    for (int j = 0;j < H;j++) {
        for (int i = 0;i < W;i++) {
            if (grid[j][i]) count++;
        }
    }

    // free
    for (int j = 0;j < H;j++) {
        delete[] grid[j];
        delete[] newgrid[j];
    }
    delete[] grid;
    delete[] newgrid;

    return count;
}


ll create_pattern(ll W, ll H, ll X, ll Y, ll T) {
    ll startY = Y - T;
    ll endY = (Y+T) < (H-1) ? (Y+T) : (H-1);
    ll w = 0;

    ll count = 0;
    for (int j = startY; j <= endY; j++) {
        for (int i = -w; i <= w; i++) {
            if (X+i < W && X+i >= 0 && j >= 0) {
                if ((i+w) % 2 == 0) count++;
            }
        }
        if (j < Y) {
            w++;
        } else {
            w--;
        }
    }

    return count;
}

ll evenodd (ll W, ll H, ll X, ll Y, ll T) {

    if (W % 2 == 0 || H % 2 == 0) {
        return W*H/2;
    }

    int numberOfOdd = 0;

    if (X % 2 == 1) {
        numberOfOdd++;
    }
    if (Y % 2 == 1) {
        numberOfOdd++;
    }
    if (T % 2 == 1) {
        numberOfOdd++;
    }

    ll a = W * H / 2;

    if (numberOfOdd % 2 == 0) {
        return a+1;
        //if (a % 2 == 0) {
            //return a+1;
        //} else {
            //return a;
        //}
    } else {
        return a;
        //if (a % 2 == 0) {
            //return a+1;
        //} else {
            //return a;
        //}
    }
}


int simulate (int W, int H, int X, int Y, int T) {
    int** grid = new int*[H];
    int** newgrid = new int*[H];
    for (int j = 0; j < H; j++) {
        grid[j] = new int[W];
        newgrid[j] = new int[W];
    }
    for (int j = 0; j < H; j++) {
        for (int i = 0; i < W; i++) {
            grid[j][i] = 0;
            newgrid[j][i] = 0;
        }
    }

    grid[Y][X] = 1;


    for (int t = 0; t < T; t++) {
        for (int j = 0; j < H; j++) {
            for (int i = 0; i < W; i++) {
                newgrid[j][i] = 0;
            }
        }
        for (int j = 0; j < H; j++) {
            for (int i = 0; i < W; i++) {
                if (grid[j][i] == 1) {
                    if (i > 0)
                        newgrid[j][i-1] = 1;
                    if (j > 0)
                        newgrid[j-1][i] = 1;
                    if (i < W-1)
                        newgrid[j][i+1] = 1;
                    if (j < H-1)
                        newgrid[j+1][i] = 1;
                }
            }
        }

        // swap
        int** temp = grid;
        grid = newgrid;
        newgrid = temp;
    }

    int count = 0;
    for (int j = 0; j < H; j++) {
        for (int i = 0; i < W; i++) {
            if (grid[j][i] == 1) count++;
        }
    }

    for (int j = 0; j < H; j++) {
        delete[] grid[j];
        delete[] newgrid[j];
    }
    delete[] grid;
    delete[] newgrid;

    return count;
}



void test () {
    for (int w = 6; w < 12; w++) {
        for (int h = 6; h < 12; h++) {
            for (int x = 1; x < 6; x++) {
                for (int y = 1; y < 6; y++) {
                    for (int t = 20; t < 28; t++) {
                        int got = (int)evenodd(w, h, x, y, t);
                        int expected = (int)simulate(w, h, x, y, t);
                        printf("%d %d %d %d %d -> got %d, expected %d  %s\n", w, h, x, y, t, got, expected, got != expected ? "ERROR!" : "");
                    }
                }
            }
        }
    }
}


int main (int argc, char** argv) {

    int N;
    scanf("%d\n", &N);


    //printf("%d\n", simulate(71,65,26,26,675));
    //test();



    for (int i = 0;i < 20;i++) {
        ll W,H,X,Y,T;
        scanf("%lld %lld %lld %lld %lld\n", &W, &H, &X, &Y, &T);
        printf("%lld\n", create_pattern(W,H,X,Y,T));
    }
    for (int i = 0;i < 10;i++) {
        ll W,H,X,Y,T;
        scanf("%lld %lld %lld %lld %lld\n", &W, &H, &X, &Y, &T);

        printf("%lld\n", evenodd(W,H,X,Y,T));
    }
    for (int i = 0;i < 10;i++) {
        ll W,H,X,Y,T;
        scanf("%lld %lld %lld %lld %lld\n", &W, &H, &X, &Y, &T);
        printf("%lld\n", create_pattern(W,H,X,Y,T));
    }
    for (int i = 0;i < 10;i++) {
        ll W,H,X,Y,T;
        scanf("%lld %lld %lld %lld %lld\n", &W, &H, &X, &Y, &T);
        printf("%lld\n", evenodd(W,H,X,Y,T));
    }



    return 0;
}
