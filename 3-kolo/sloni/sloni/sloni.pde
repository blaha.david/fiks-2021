
final int W = 71;
final int H = 65;
final int X = 26;
final int Y = 26;
final int T = 675;//921929865

boolean[][] grid = new boolean[H][];
boolean[][] newgrid = new boolean[H][];

float tileSize;

int currentT = 0;

int stepDelay = 10;

void setup () {
  size(500, 500);

  for (int j = 0; j < H; j++) {
    grid[j] = new boolean[W];
    newgrid[j] = new boolean[W];
  }

  grid[Y][X] = true;

  tileSize = min((float)width/W, (float)height/H);

  //println(simulate(W, H, X, Y, T));
  test();
}


void draw() {
  //if (frameCount % stepDelay == 0) step();
  for (int j = 0; j < H; j++) {
    for (int i = 0; i < W; i++) {
      //stroke(0);
      noStroke();
      if (grid[j][i]) {
        fill(0, 255, 255, currentT % 2 == 0 ? 255 : 120);
      } else {
        fill(0);
      }
      rect(i*tileSize, j*tileSize, tileSize, tileSize);
    }
  }
}


void step () {
  for (int j = 0; j < H; j++) {
    for (int i = 0; i < W; i++) {
      newgrid[j][i] = false;
    }
  }
  for (int j = 0; j < H; j++) {
    for (int i = 0; i < W; i++) {
      if (grid[j][i]) {
        if (i > 0)
          newgrid[j][i-1] = true;
        if (j > 0)
          newgrid[j-1][i] = true;
        if (i < W-1)
          newgrid[j][i+1] = true;
        if (j < H-1)
          newgrid[j+1][i] = true;
      }
    }
  }

  // swap
  boolean[][] temp = grid;
  grid = newgrid;
  newgrid = temp;

  currentT++;
}


int simulate (int W, int H, int X, int Y, int T) {
  boolean[][] grid = new boolean[H][];
  boolean[][] newgrid = new boolean[H][];
  for (int j = 0; j < H; j++) {
    grid[j] = new boolean[W];
    newgrid[j] = new boolean[W];
  }

  grid[Y][X] = true;


  for (int t = 0; t < T; t++) {
    for (int j = 0; j < H; j++) {
      for (int i = 0; i < W; i++) {
        newgrid[j][i] = false;
      }
    }
    for (int j = 0; j < H; j++) {
      for (int i = 0; i < W; i++) {
        if (grid[j][i]) {
          if (i > 0)
            newgrid[j][i-1] = true;
          if (j > 0)
            newgrid[j-1][i] = true;
          if (i < W-1)
            newgrid[j][i+1] = true;
          if (j < H-1)
            newgrid[j+1][i] = true;
        }
      }
    }

    // swap
    boolean[][] temp = grid;
    grid = newgrid;
    newgrid = temp;
  }

  int count = 0;
  for (int j = 0; j < H; j++) {
    for (int i = 0; i < W; i++) {
      if (grid[j][i]) count++;
    }
  }

  return count;
}


void createPattern () {
  int startY = Y - T;
  int startX = X - T;
  //int endY = min(Y + T, H-1);
  int endY = (Y+T) < (H-1) ? (Y+T) : (H-1);
  int endX = (X+T) < (W-1) ? (X+T) : (W-1);
  int w = 0;


  for (int j = startY; j <= endY; j++) {
    //println();
    for (int i = -w; i <= w; i++) {
      //println(String.format("[%d,%d]: (%d %% 2) == (%d %% 2) %s", i, j, (i), (j-startY), (i < W && i >= 0 && j >= 0) ? "in" : ""));
      if (X+i < W && X+i >= 0 && j >= 0) {
        grid[j][X+i] = (i+w) % 2 == 0;
        //grid[j][i] = true;
      }
    }
    if (j < Y) {
      w++;
    } else {
      w--;
    }
  }
  int count = 0;
  for (int j = 0; j < H; j++) {
    for (int i = 0; i < W; i++) {
      if (grid[j][i]) count++;
    }
  }
  println("count=" + count);
}

int evenodd (int W, int H, int X, int Y, int T) {

  if (W % 2 == 0 || H % 2 == 0) {
    return W*H/2;
  }

  int numberOfOdd = 0;

  if (X % 2 == 1) {
    numberOfOdd++;
  }
  if (Y % 2 == 1) {
    numberOfOdd++;
  }
  if (T % 2 == 1) {
    numberOfOdd++;
  }

  int a = W * H / 2;

  if (numberOfOdd % 2 == 0) {
    if (a % 2 == 0) {
      return a+1;
    } else {
      return a;
    }
  } else {
    if (a % 2 == 0) {
      return a;
    } else {
      return a+1;
    }
  }
}



void keyPressed () {
  if (key == ' ') step();
  if (key == 'c') createPattern();

  if (key == 'a') {
    for (int i = 0; i < T; i++) {
      step();
    }
  }
}

void test () {
  for (int w = 6; w < 10; w++) {
    for (int h = 6; h < 10; h++) {
      for (int x = 1; x < 6; x++) {
        for (int y = 1; y < 6; y++) {
          for (int t = 20; t < 28; t++) {
            int got = evenodd(w, h, x, y, t);
            int expected = simulate(w, h, x, y, t);
            println(String.format("%d %d %d %d %d -> got %d, expected %d  %s", w, h, x, y, t, got, expected, got != expected ? "ERROR!" : ""));
          }
        }
      }
    }
  }
}



//final int W = 9;
//final int H = 3;
//final int X = 4;
//final int Y = 2;
//final int T = 7;

//final int W = 9;
//final int H = 8;
//final int X = 6;
//final int Y = 2;
//final int T = 2;

//final int W = 6;
//final int H = 5;
//final int X = 1;
//final int Y = 1;
//final int T = 2;
