//final int W = 9;
//final int H = 3;
//final int X = 4;
//final int Y = 2;
//final int T = 7;

////final int W = 9;
////final int H = 8;
////final int X = 6;
////final int Y = 2;
////final int T = 2;

////final int W = 6;
////final int H = 5;
////final int X = 1;
////final int Y = 1;
////final int T = 2;

//boolean[][] grid = new boolean[H][];
//boolean[][] newgrid = new boolean[H][];

//float tileSize;

//int currentT = 0;

//int stepDelay = 10;

//void setup () {
//  size(800, 800);

//  for (int j = 0; j < H; j++) {
//    grid[j] = new boolean[W];
//    newgrid[j] = new boolean[W];
//  }

//  grid[Y][X] = true;

//  tileSize = min((float)width/W, (float)height/H);
//}


//void draw() {
//  //if (frameCount % stepDelay == 0) step();
//  for (int j = 0; j < H; j++) {
//    for (int i = 0; i < W; i++) {
//      //stroke(0);
//      noStroke();
//      if (grid[j][i]) {
//        fill(0, 255, 255, currentT % 2 == 0 ? 255 : 120);
//      } else {
//        fill(0);
//      }
//      rect(i*tileSize, j*tileSize, tileSize, tileSize);
//    }
//  }
//}


//void step () {
//  for (int j = 0; j < H; j++) {
//    for (int i = 0; i < W; i++) {
//      newgrid[j][i] = false;
//    }
//  }
//  for (int j = 0; j < H; j++) {
//    for (int i = 0; i < W; i++) {
//      if (grid[j][i]) {
//        if (i > 0)
//          newgrid[j][i-1] = true;
//        if (j > 0)
//          newgrid[j-1][i] = true;
//        if (i < W-1)
//          newgrid[j][i+1] = true;
//        if (j < H-1)
//          newgrid[j+1][i] = true;
//      }
//    }
//  }

//  // swap
//  boolean[][] temp = grid;
//  grid = newgrid;
//  newgrid = temp;

//  currentT++;
//}


//void createPattern () {
//  int startY = Y - T;
//  int startX = X - T;
//  //int endY = min(Y + T, H-1);
//  int endY = (Y+T) < (H-1) ? (Y+T) : (H-1);
//  int endX = (X+T) < (W-1) ? (X+T) : (W-1);
//  int w = 0;


//  for (int j = startY; j <= endY; j++) {
//    println();
//    for (int i = -w; i <= w; i++) {
//      println(String.format("[%d,%d]: (%d %% 2) == (%d %% 2) %s", i, j, (i), (j-startY), (i < W && i >= 0 && j >= 0) ? "in" : ""));
//      if (X+i < W && X+i >= 0 && j >= 0) {
//        grid[j][X+i] = (i+w) % 2 == 0;
//        //grid[j][i] = true;
//      }
//    }
//    if (j < Y) {
//      w++;
//    } else {
//      w--;
//    }
//  }
//  int count = 0;
//  for (int j = 0; j < H; j++) {
//    for (int i = 0; i < W; i++) {
//      if (grid[j][i]) count++;
//    }
//  }
//  println("count=" + count);
//}

//void keyPressed () {
//  if (key == ' ') step();
//  if (key == 'c') createPattern();
//}
