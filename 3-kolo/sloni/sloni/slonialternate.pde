//final int W = 10;
//final int H = 10;
//final int X = 3;
//final int Y = 2;
//final int T = 5;

//int[][] grid = new int[H][];
//int[][] newgrid = new int[H][];

//int tileSize;

//int currentT = 0;

//int stepDelay = 10;

//void setup () {
//  size(800,800);
  
//  for (int j = 0;j < H;j++) {
//    grid[j] = new int[W];
//    newgrid[j] = new int[W];
//  }
  
//  grid[Y][X] = 1;
  
//  tileSize = min(width/W, height/H);
//}


//void draw() {
//  if (frameCount % stepDelay == 0) step();
//  for (int j = 0;j < H;j++) {
//    for (int i = 0;i < W;i++) {
//      int cell = grid[j][i];
//      stroke(255);
//      fill(0);
//      rect(i*tileSize, j*tileSize, tileSize, tileSize);
//      if (cell == 1)  {
//        fill(0,255,255, currentT % 2 == 0 ? 255 : 120);
//      } else if (cell == 2) {
//        fill(255,127,0, currentT % 2 == 0 ? 120 : 255);
//      }
//      rect(i*tileSize, j*tileSize, tileSize, tileSize);
//    }
//  }
//}


//void step () {
//  print("T=" + currentT + ": ");
  
//  int count = 0;
  
//  for (int j = 0;j < H;j++) {
//    for (int i = 0;i < W;i++) {
//      boolean even = currentT % 2 == 0;
//      if (grid[j][i] == (even ? 1 : 2)) {
//        count++;
//        newgrid[j][i] = grid[j][i];
//        if (i > 0) newgrid[j][i-1] = even ? 2 : 1;
//        if (j > 0) newgrid[j-1][i] = even ? 2 : 1;
//        if (i < W-1) newgrid[j][i+1] = even ? 2 : 1;
//        if (j < H-1) newgrid[j+1][i] = even ? 2 : 1;
//      }
//    }
//  }
  
//  println(count);
  
//  grid = newgrid;
//  newgrid = new int[H][];
//  for (int j = 0;j < H;j++) {
//    newgrid[j] = new int[W];
//  }
  
//  currentT++;
//}


//void keyPressed () {
//  if (key == ' ') step();
//}
