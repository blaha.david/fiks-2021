#! /usr/bin/env python3

from pprint import pprint

import sys

#import line_profiler
#import atexit
#profile = line_profiler.LineProfiler()
#atexit.register(profile.print_stats)

import time
start_time = time.time()


def calculate(T: int) -> int:

    current_even = 0
    current_odd = 0
    prev_even = 1
    prev_odd = 0

    for t in range(1,T+1):
        print(f"{t=}, {current_even=}, {current_odd=}, {prev_even=}, {prev_odd=}")
        even = t % 2 == 0

        if even:
            # new_even = t*4 + prev_even
            # prev_even = current_even
            # current_even = new_even
            current_even = t*4 + prev_even
            prev_even = current_even
        else:
            # new_odd = t*4 + prev_odd
            # prev_odd = current_odd
            # current_odd = new_odd
            current_odd = t*4 + prev_odd
            prev_odd = current_odd

    return current_even if T % 2 == 0 else current_odd


def calculate_simple(W: int, H: int) -> int:

    return W*H//2

def calculate_evenodd(W: int, H: int, X: int, Y: int, T: int) -> int:

    if W % 2 == 0 or H % 2 == 0:
        return calculate_simple(W,H)


    number_of_odd = 0

    if X % 2 == 1:
        number_of_odd += 1
    if Y % 2 == 1:
        number_of_odd += 1
    if T % 2 == 1:
        number_of_odd += 1

    a = W*H//2
    if number_of_odd % 2 == 0:
        if a % 2 == 0:
            return a+1
        else:
            return a
    else:
        if a % 2 == 0:
            return a
        else:
            return a+1

def solve_grid(W: int, H: int, X: int, Y: int, T: int) -> int:
    grid = [[False for i in range(W)] for j in range(H)]

    grid[Y][X] = True

    current_t = 0
    while current_t < T:
        newgrid = [[False for i in range(W)] for j in range(H)]
        for j in range(H):
            for i in range(W):
                if grid[j][i]:
                    if i > 0:
                        newgrid[j][i-1] = True
                    if j > 0:
                        newgrid[j-1][i] = True
                    if i < W-1:
                        newgrid[j][i+1] = True
                    if j < H-1:
                        newgrid[j+1][i] = True
        grid = newgrid
        current_t += 1


    count = 0
    for j in range(H):
        for i in range(W):
            if grid[j][i]:
                count += 1
    return count

def solve_pattern(W: int, H: int, X: int, Y: int, T: int) -> int:
    startY = Y - T
    endY = (Y+T) if (Y+T) < (H-1) else (H-1)
    w = 0

    if startY < 0:
        w = -startY
        startY = 0

    count = 0
    for j in range(startY, endY+1):
        for i in range(-w, w+1):
            if X+i < W and X+i >= 0:
                if ((X+i) % 2 == (T % 2)) ^ (j % 2 == 0):
                    count += 1
        if j < Y:
            w += 1
        else:
            w -= 1

    return count



def solve_pairs(W: int, H: int, X: int, Y: int, T: int) -> int:
    start = (X,Y)
    set1: set = {start}
    set2: set = set()

    currentT = 0

    while currentT < T:
        if currentT % 2 == 0:
            for (x,y) in set1:
                if x > 0:
                    set2.add((x-1,y,))
                if y > 0:
                    set2.add((x,y-1,))
                if x < W-1:
                    set2.add((x+1,y,))
                if y < H-1:
                    set2.add((x,y+1,))
            set1.clear()
        else:
            for (x,y) in set2:
                if x > 0:
                    set1.add((x-1,y,))
                if y > 0:
                    set1.add((x,y-1,))
                if x < W-1:
                    set1.add((x+1,y,))
                if y < H-1:
                    set1.add((x,y+1,))
            set2.clear()

        currentT += 1

    return len(set1 if currentT % 2 == 0 else set2)



if __name__ == '__main__':
    input()

    for _ in range(20):
        W, H, X, Y, T = [ int(x) for x in input().split(' ') ]
        print(solve_pattern(W, H, X, Y, T))

    for _ in range(10):
        W, H, X, Y, T = [ int(x) for x in input().split(' ') ]
        print(calculate_evenodd(W, H, X, Y, T))

    for _ in range(10):
        W, H, X, Y, T = [ int(x) for x in input().split(' ') ]
        print(solve_pattern(W, H, X, Y, T))

    for _ in range(10):
        W, H, X, Y, T = [ int(x) for x in input().split(' ') ]
        print(calculate_evenodd(W, H, X, Y, T))




