#! /usr/bin/env python3

class Zvire():
    def __init__(self, dravost: int, index: int):
        self.dravost = dravost
        self.index = index

def zadani():
    n = int(input())  # n
    dravosti = input().split(' ')
    diff = int(input())

    zvirata = []
    for i in range(n):
        zvirata.append(Zvire(int(dravosti[i]), i+1))

    zvirata.sort(key=lambda x: x.dravost)

    pavilony = []
    current = -1
    nejmensiDravost = -999999

    for zvire in zvirata:
        if abs(zvire.dravost - nejmensiDravost) > diff:
            current += 1
            pavilony.append([zvire])
            nejmensiDravost = zvire.dravost
        else:
            pavilony[current].append(zvire)

    for pavilon in pavilony:
        print("({})".format(' '.join([str(zvire.index) for zvire in sorted(pavilon, key=lambda x: x.index)])))

if __name__ =='__main__':
    zadani()
