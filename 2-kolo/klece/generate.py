#! /usr/bin/env python3

import sys
import random


def generate(min, max):
    n = random.randint(min, max)
    print(n)


    print(' '.join([str(random.randint(n//2, n*2)) for _ in range(n)]))

    print(random.randint(n//2, n))


if __name__ == '__main__':
    min = 5
    max = 10

    if len(sys.argv) == 2:
        min = int(sys.argv[1])
        max = min+5
    if len(sys.argv) == 3:
        min = int(sys.argv[1])
        max = int(sys.argv[2])
    generate(min, max)
