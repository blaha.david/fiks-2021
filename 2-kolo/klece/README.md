
# Klece


## Popis algoritmu

Algoritmus vezme všechna zvířata a seřadí je podle jejich dravosti od nejmenší po největší.
Uloží si dravost prvního zvířete.
Vytvoří list pavilonů. A přidá do něho první prázdný pavilon
Poté postupně projíždí všechna zvířata
a do aktuálního pavilonu přidává zvířata,
dokud rozdíl dravosti dalšího zvířete a uložené dravosti není větší než povolená.
Poté vytvoří nový pavilon a proces opakuje.


## Asymptotická složitost

Seřazení zvířat podle dravosti je `O(n*log(n))`

Poté algorismus projde všechna zvířata - `O(n)`

Celkem tedy `O(n*log(n))`
