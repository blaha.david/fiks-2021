# FIKS 2021

## 2. kolo

### [Dělníci](delnici/) `10/10`
Odpověz Sfinze!
[download pdf](https://fiks.fit.cvut.cz/files/tasks/season8/round2/delnici.pdf)


### [Klece](klece/) `6/10`
Zamysli se!
[download pdf](https://fiks.fit.cvut.cz/files/tasks/season8/round2/klece.pdf)


### [Prohlídkové okruhy](prohlidkove-okruhy/) `7/10`
Rozmysli, popiš a naprogramuj!
[download pdf](https://fiks.fit.cvut.cz/files/tasks/season8/round2/prohlidkove-okruhy.pdf)


