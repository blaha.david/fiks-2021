#!/usr/bin/env bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color


if [ $# -eq 1 ];then
    diff --strip-trailing-cr tests/expected$1.txt tests/output$1.txt >/dev/null
    if [ $? -eq 0 ]; then
        echo -e "${GREEN}Passed${NC}"
    else
        echo -e "${RED}Failed:${NC}"
        git diff --color-words --minimal --no-index tests/expected$1.txt tests/output$1.txt | cat
    fi
    exit
fi

for i in {1..5}; do
    echo -e "\nRunning test #$i:"
    ./solve.py < tests/vstup$i.txt > tests/output$i.txt 2>&1
    diff --strip-trailing-cr tests/expected$i.txt tests/output$i.txt >/dev/null
    if [ $? -eq 0 ]; then
        echo -e "${GREEN}Passed${NC}"
    else
        echo -e "${RED}Failed:${NC}"
        git diff --color-words --minimal --no-index tests/expected$i.txt tests/output$i.txt | cat
    fi
done


# NAME="rustsolve"
#
# cargo build --manifest-path $NAME/Cargo.toml
#
# if [ $? -ne 0 ]; then
#     echo "Compile error"
#     exit 1
# fi
# read
#
# for i in {1..2}; do
#     echo "Running test #$i:"
#     $NAME/target/debug/$NAME < tests/vstup$i.txt > tests/output$i.txt
#     git diff --color-words --no-index tests/expected$i.txt tests/output$i.txt
# done
#
