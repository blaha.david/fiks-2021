#! /usr/bin/env python3
from pprint import pprint


# NOTE:
# Jazyk = Node, Vrchol
# Prekladatel = Edge, Cesta
#
# Seradit prekladatele podle ceny
# do cest davat jen nizsi cenu
#

class Prekladatel():
    def __init__(self, cena: int, jazyky: list[dict]):
        self.cena = cena
        self.jazyky = jazyky


class Node():
    def __init__(self, jazyk, edges):
        self.name = jazyk
        self.edges = [e for e in edges]  # Prepare

        self.previous = None  # Dijkstra
        self.value = 99999999  # Dijkstra

    def __repr__(self):
        return f"{self.name} [{', '.join(str(edge) for edge in self.edges)}]"


class Edge():
    def __init__(self, weight: int, other: Node):
        self.weight = weight
        self.other = other  # Node

    def __repr__(self):
        return f" -> {self.other.name} ({self.weight})"

    def __str__(self):
        return self.__repr__()


def solve(start_jazyk, end_jazyk, graph, prekladatele):

    # Generate graph:

    for prek in sorted(prekladatele, key=lambda x: x.cena):
        for i, jazyk in enumerate(prek.jazyky):
            for j in range(i+1, len(prek.jazyky)):
                other = prek.jazyky[j]

                graph[jazyk].edges.append(Edge(prek.cena, graph[other]))
                graph[other].edges.append(Edge(prek.cena, graph[jazyk]))

    #
    # Find best route:
    # Dijkstra algorithm
    open_set = [graph[start_jazyk]]

    open_set[0].value = 0

    while len(open_set) > 0:
        node = open_set[0]
        for edge in node.edges:
            new_val = node.value + edge.weight
            if new_val < edge.other.value:
                edge.other.value = new_val
                edge.other.previous = node
                open_set.append(edge.other)
        open_set.remove(node)

    # Reverse step to solution:
    solution = []

    current = graph[end_jazyk]
    while True:
        solution.append(current.name)
        if current.previous is None:
            break
        current = current.previous

    if len(solution) == 1 and start_jazyk != end_jazyk:
        print('Takove prekladatele nemame.')

    else:
        print('To nas bude stat {},-.'.format(graph[end_jazyk].value))
        print('Pocet prekladu: {}.'.format(len(solution)-1))
        print('\n'.join(reversed(solution)))


def zadani():
    nodes = {}  # Jazyky = Dictionary<string, Node>
    N = int(input())
    for _ in range(N):
        jazyk = input()
        nodes[jazyk] = Node(jazyk, [])

    prekladatele = []
    P = int(input())
    for _ in range(P):
        line = input().split(' ')
        cena = int(line[1])
        jazyky = line[2:]
        prekladatele.append(Prekladatel(cena, jazyky))

    start_jazyk, end_jazyk = input().split(' ')

    solve(start_jazyk, end_jazyk, nodes, prekladatele)


if __name__ == '__main__':
    T = int(input())
    for _ in range(T):
        zadani()
