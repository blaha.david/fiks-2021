#!/usr/bin/env python3

import sys
import random

def generate(M: int, N: int, O: int) -> None:
    print('{} {} {}'.format(M, N, O))
    cesty = []
    while len(cesty) < N:
        A = random.randint(1,M)
        B = random.randint(1,M)
        if A == B:
            continue
        cesta = (A,B)
        if cesta not in cesty:
            cesty.append(cesta)

    for cesta in cesty:
        print('{} {}'.format(cesta[0], cesta[1]))

    dotazy = []
    while len(dotazy) < O:
        A = random.randint(1,M)
        B = random.randint(1,M)
        if A == B:
            continue
        dotaz = (A,B)
        if dotaz not in dotazy:
            dotazy.append(dotaz)

    for dotaz in dotazy:
        print('{} {}'.format(dotaz[0], dotaz[1]))


if __name__ == "__main__":
    M = 5
    N = 4
    O = 5

    if len(sys.argv) == 4:
        M = int(sys.argv[1])
        N = int(sys.argv[2])
        O = int(sys.argv[3])

    generate(M, N, O)

