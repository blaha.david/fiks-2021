
#  Prohlídkové okruhy


## Popis algoritmu

Algoritmus začne tím, že vytvoří objekt pro každou křižovatku.
Ten obsahuje list všech křižovatek, které jsou s ní přímo spojené, který se naplní v další části.
Taky obsahuje bitset všech křižovatek, jestli je možné se z této křižovatky dostat na danou křižovatku, který se naplní v hlavní části algoritmu.
To má výhodu tu, že poté jsou už všechy dotazy jenom jednoduchý lookup a nemusí se nic počítat znovu a znovu.

Algoritmus projede všechny křižovatky a spočítá, na které je možné se dostat.
Funguje to tak, že má tzv. openset, kde má uložené všechny křižovatky, na které se má ještě podívat.
Openset začíná se všemi přímo připojenými křižovatkami.

Algoritmus se opakuje dokud ještě zbývá nějaká křižovatka v opensetu:
Vezmi (třeba poslední - protože je to nejjednodušší) křižovatku,
odstraň ji z opensetu a nastav, že je z této křižovatky dosažitelná (closedset).
Potom všechny křižovatky které jsou s ní přímo spojené dej do opensetu,
ale jenom pokud ještě nejsou v opensetu nebo closedsetu.

Dotaz je poté už jenom lookup v bitsetu dosažitelných křižovatek dané křižovatky.



## Asymptotická složitost

Přiřazování cest ke křižovatkám je `O(N)`. (Jedno přidání do listu je `O(1)`)

Algoritmus projede všechny křižovatky a pro každou křižovatku v nejhorším případě projede všechny cesty.
Takže složitost algoritmu je `O(M*N)`.

Dotazy jsou poté `O(1)`

