#!/usr/bin/env bash

NUM=3

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

for i in $( eval echo {1..$NUM} ); do
    echo -e "\nRunning test #$i:"
    START=$(date +%s%N | cut -b1-13)
    ./solve.py < tests/vstup$i.txt > tests/output$i.txt 2>&1
    END=$(date +%s%N | cut -b1-13)
    diff tests/expected$i.txt tests/output$i.txt >/dev/null
    if [ $? -eq 0 ]; then
        echo -e "${GREEN}Passed${NC}"
    else
        echo -e "${RED}Failed:${NC}"
        #echo -e `git diff --color-words --minimal --no-index tests/expected$i.txt tests/output$i.txt`
        git diff --color-words --minimal --no-index tests/expected$i.txt tests/output$i.txt | cat
    fi
    echo -e "Took: $((END-START))ms"
done


# NAME="rustsolve"
#
# cargo build --manifest-path $NAME/Cargo.toml
#
# if [ $? -ne 0 ]; then
#     echo "Compile error"
#     exit 1
# fi
# read
#
# for i in {1..2}; do
#     echo "Running test #$i:"
#     $NAME/target/debug/$NAME < tests/vstup$i.txt > tests/output$i.txt
#     git diff --color-words --no-index tests/expected$i.txt tests/output$i.txt
# done
#
