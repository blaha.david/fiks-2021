#!/usr/bin/env python3

class Node():
    def __init__(self, i: int, length: int) -> None:
        self.i = i # index (id)
        self.edges: list[int] = [] # prime spojeni
        self.connections: list[bool] = [False]*length # ktere krizovatky jsou dosazitelne z teto krizovatky

    def calculate_connections(self, nodes: list) -> None:
        openset = [ nodes[edge] for edge in self.edges ]
        self.connections[self.i] = True # closedset

        while len(openset) > 0:
            node = openset.pop()
            self.connections[node.i] = True # closedset
            for edge in node.edges:
                if not self.connections[edge] and nodes[edge] not in openset:
                    openset.append(nodes[edge])

if __name__ == "__main__":
    M, N, O = [ int(x) for x in input().split(' ') ]

    # Krizovatky
    nodes = [ Node(i, M) for i in range(M) ] # generate M nodes

    # Cesty
    for _ in range(N):
        P, Q = [ int(x)-1 for x in input().split(' ') ]
        nodes[P].edges.append(Q)

    # Hlavni cast algoritmu:
    for node in nodes: # M
        node.calculate_connections(nodes)

    # Dotazy:
    for _ in range(O):
        A, B = [ int(x)-1 for x in input().split(' ') ]
        if nodes[A].connections[B]:
            print("Cesta existuje")
        else:
            print("Cesta neexistuje")


