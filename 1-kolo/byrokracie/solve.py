#!/usr/bin/env python3

import time
import sys
import threading
import inspect

DEBUG = False

class Povoleni():

    def __init__(self, cislo_povoleni: int):
        self.cislo_povoleni = cislo_povoleni
        self.zavislosti = []
        self.linked = False

        self.umoznuje = [] # kdo je zavisly na nem

    def __str__(self):
        return f"Povoleni #{self.cislo_povoleni}: {'linked' if self.linked else 'unlinked'}, zavisle na: {str(self.zavislosti)}, umoznuje: {str(self.umoznuje)}"

    def __repr__(self) -> str:
        return self.__str__()


def make_links(povoleni: list[Povoleni], current: int):
    povoleni[current].linked = True
    for povol in povoleni[current].zavislosti:
        if not povoleni[povol].linked:
            make_links(povoleni, povol)


# vytvor opacne spojeni aby slo jit pozpatku
# nevytvari spojeni, ktera nejsou potreba (6->7, 7->6 pokud 6 ani 7 nejsou potreba na nic jineho)
def build_tree(povoleni: list[Povoleni], current: int, occurences: list[int]):
    if occurences[current] > 5: # prevent stack overflow and max recursion errors
        return
    occurences[current] += 1
    # print(f"Stack: {len(inspect.stack())}, {current=}, {occurences=}")
    for zavislost in povoleni[current].zavislosti:
        if current not in povoleni[zavislost].umoznuje:
            povoleni[zavislost].umoznuje.append(current)
        try:
            build_tree(povoleni, zavislost, occurences)
        except RecursionError:
            return

def iterace(open_set: list[int], vysledek: list[int], povoleni: list[Povoleni]):
    for povol in open_set:
        # zkontrolovat, ze povoleni uz ma ve vysledku vsechny svoje zavislosti
        nema = False
        for zavis in povoleni[povol].zavislosti:
            if zavis not in vysledek:
                nema = True
        if nema:
            continue

        vysledek.append(povol) # (vysledek slouzi jako closed set)
        open_set.remove(povol) # odebrat z open setu

        for dalsi in povoleni[povol].umoznuje: # pokracujem nahoru ve strome
            if dalsi not in vysledek and dalsi not in open_set: # ignorovat co uz bylo
                open_set.append(dalsi)


class SomeCallable:

    def main(self):

        # Na prvním řádku je přirozené číslo 1 ≤ T ≤ 100 značící počet zadání, která budou následovat.
        T = int(input())
        # print(f"{T=}")

        for tt in range(T):

            # time.sleep(0.02)

            if DEBUG:
                print(f"\n\n\n === Řešeni #{tt+1}: ===")

            # Na prvním řádku každého zadání jsou tři přirozená čísla 0 ≤ P ≤ 103 , 0 ≤ Z ≤ 106 a
            # 0 ≤ n < P oddělená mezerou, kde P značí počet povolení a n značí číslo povolení ke stavbě.
            P, Z, n = [int(i) for i in input().split(' ')]
            if DEBUG:
                print(f"{P=}, {Z=}, {n=}")


            # Následuje Z řádků, na i-tém řádku zadání jsou dvě přirozená čísla 0 ≤ ai ≤ P a 0 ≤ bi ≤ P.
            # Tato dvojice reprezentuje závislost povolení číslo ai na povolení číslo bi – jinými slovy,
            # na vyřízení povolení ai je nejprve potřeba vyřídit povolení bi .
            povoleni = [Povoleni(p) for p in range(P)]

            povoleni[n] = Povoleni(n)
            for _ in range(Z):
                a, b = [int(i) for i in input().split(' ')]
                povoleni[a].zavislosti.append(b)


            make_links(povoleni, n)

            # for povol in povoleni:
                # print(povol)


            # Na výstupu je jeden řádek pro každé zadání.

            # Pokud lze všechny závislosti mezi povoleními uspokojit a vyřídit povolení číslo n, pak
            # výstupní řádek začíná slovy pujde to. Po nich následuje m mezerou oddělených přirozených
            # čísel p1 , p2 , . . . , pm (pm = n) značící čísla povolení v pořadí,
            # v jakém je možné je vyřídit,
            # aby při vyřizování konkrétního povolení byly už všechny jeho závislosti vyřízené.
            # Řešení minimalizuje m, tj. posloupnost povolení neobsahuje žádná,
            # která k vyřízení povolení n nejsou opravdu potřeba.
            # Nelze-li závislosti uspokojit, je výstupem pouze slovo ajajaj.

            # print('building tree')
            occurences = [0]*P
            build_tree(povoleni, n, occurences)

            if DEBUG:
                for povol in povoleni:
                    print(povol)


            # open set reseni

            vysledek: list[int] = []

            open_set: list[int] = []

            # inicializace
            for povol in povoleni:
                if len(povol.zavislosti) == 0 and povol.linked: # zacit od konce - od toho, ktery nema zadne zavislosti, pridat pouze pokud je spojena s hlavnim stromem
                    open_set.append(povol.cislo_povoleni)

            # print("RESENI:")
            open_set_copy = open_set.copy()
            infinite = False

            while len(open_set) > 0:
                # print(f"{open_set=}, {vysledek=}")
                # print(f"ITERACE {open_set=}")
                iterace(open_set, vysledek, povoleni)
                if open_set == open_set_copy: # no change
                    infinite = True
                    break
                open_set_copy = open_set.copy()

            # print(f"DONE: {vysledek=}")
            if len(vysledek) == 0 or infinite:
                print("ajajaj")
            else:
                print("pujde to", " ".join([str(v) for v in vysledek]))

            del povoleni
            del vysledek
            del open_set
            del P, Z, n

    def __call__(self):
        self.main()





if __name__ == "__main__":
    sys.setrecursionlimit(200000)
    threading.stack_size(0x8000000)
    t = threading.Thread(target=SomeCallable())
    t.start()
    t.join()
