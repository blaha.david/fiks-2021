#!/usr/bin/env python3



def main():

    # Na prvním řádku je přirozené číslo 1 ≤ T ≤ 100 značící počet zadání, která budou následovat.
    T = int(input())
    # print(f"{T=}")

    for tt in range(T):

        # print(f"\n\n\n === Řešeni #{tt+1}: ===")

        # Na prvním řádku každého zadání se nachází celé číslo 1 ≤ N ≤ 200 značící počet druhů
        # nakupovaných zvířat. Následuje N řádků s informacemi o jednotlivých zvířatech.
        N = int(input())
        # print(N)

        # Každý řádek obsahuje tři mezerou oddělené údaje:
        # jednoslovný název zvířete, počet objednaných jedinců
        # a výměr výběhu na jednoho jedince v metrech čtverečních.
        # Názvy zvířat jsou unikátní, počet jedinců od jednoho druhu nepřesáhne 1 000
        # a výměr na jedno zvíře je nejvýše 10 000 m2.
        sum = 0
        for _ in range(N):
            line = input().split(' ')
            # nazev = line[0] # neni potreba
            pocet = int(line[1])
            vymer_vybehu = int(line[2])
            sum += pocet*vymer_vybehu

        # Výstupem je T čísel: pro každé zadání je vypsána celková plocha
        # potřebná pro výběhy všech zvířat v seznamu.
        print(sum)


if __name__ == "__main__":
    main()
