#! /usr/bin/env python3
import random
from unidecode import unidecode
import sys

# 52
ZVIRATA = ['Beran', 'Blecha', 'Bobr', 'Daněk', 'Datel', 'Dudek', 'Havran', 'Holub', 'Hrdlička', 'Jelen', 'Ježek', 'Komár', 'Kos', 'Koza', 'Kočka', 'Krtek', 'Krysa', 'Králík', 'Krůta', 'Křeček', 'Lev', 'Liška', 'Medvěd', 'Motýl', 'Moucha', 'Orel', 'Papoušek', 'Pelikán', 'Potkan', 'Poštolka', 'Ryba', 'Rys', 'Slavík', 'Slon', 'Sojka', 'Sokol', 'Sova', 'Srnka', 'Stehlík', 'Sysel', 'Sýkora', 'Veverka', 'Vlk', 'Vrána', 'Zajíc', 'Čejka', 'Čermák', 'Čáp', 'Čížek', 'Špaček', 'Šváb', 'Žába']

# 80
JMENA = ['Adam', 'Adéla', 'Alžběta', 'Amálie', 'Aneta', 'Anežka', 'Anna', 'Antonín', 'Barbora', 'Daniel', 'David', 'Denisa', 'Dominik', 'Elen', 'Elena', 'Eliška', 'Ella', 'Ema', 'Emma', 'Filip', 'František', 'Gabriela', 'Hana', 'Jakub', 'Jan', 'Jaroslav', 'Jiří', 'Jonáš', 'Josef', 'Julie', 'Jáchym', 'Karolína', 'Kateřina', 'Klára', 'Kristýna', 'Kryštof', 'Laura', 'Lucie', 'Lukáš', 'Magdaléna', 'Marek', 'Marie', 'Markéta', 'Martin', 'Matyáš', 'Matěj', 'Michael', 'Michaela', 'Michal', 'Mikuláš', 'Natálie', 'Nela', 'Nikol', 'Nikola', 'Oliver', 'Ondřej', 'Patrik', 'Pavel', 'Petr', 'Richard', 'Rozálie', 'Samuel', 'Sebastian', 'Simona', 'Sofie', 'Sára', 'Tadeáš', 'Tereza', 'Tobiáš', 'Tomáš', 'Vanesa', 'Veronika', 'Viktor', 'Viktorie', 'Vojtěch', 'Václav', 'Vít', 'Zuzana', 'Šimon', 'Štěpán']

# print(len(ZVIRATA))
# print(len(JMENA))

zps_min = 0
zps_max = 0
if len(sys.argv) >= 3:
    N = min(int(sys.argv[1]), len(ZVIRATA))
    M = min(int(sys.argv[2]), len(JMENA))
    if len(sys.argv) >= 4:
        zps_min = int(sys.argv[3]) # zvirata per sponzor
    if len(sys.argv) >= 5:
        zps_max = int(sys.argv[4]) # zvirata per sponzor
else:
    N = random.randint(1,min(100, len(ZVIRATA)))
    M = random.randint(1,min(200, len(JMENA)))

print(N, M)

for i in range(N):
    zvire = random.choice(ZVIRATA)
    ZVIRATA.remove(zvire)
    print(i, unidecode(zvire).lower())

for m in range(M):
    jmeno = random.choice(JMENA)
    JMENA.remove(jmeno)
    if zps_min == 0:
        pocet = random.randint(1, random.randint(1,N-1))
        # pocet = random.randint(1, N-2)
    else:
        pocet = random.randint(zps_min, zps_max)
    sponzorovani = []
    for _ in range(pocet):
        zvire = str(random.randint(0, N-1))
        if zvire not in sponzorovani:
            sponzorovani.append(zvire)
    print(unidecode(jmeno), len(sponzorovani), ' '.join(sponzorovani))
