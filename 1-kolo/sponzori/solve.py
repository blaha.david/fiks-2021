#!/usr/bin/env python3
from copy import deepcopy
import logging

log = logging.getLogger("solve.py")
log.addHandler(logging.StreamHandler())
log.setLevel(logging.INFO)

class Zvire():
    """ Kazde zvire ma svoje id a nazev """
    def __init__(self, id: int, nazev: str) -> None:
        self.id = id
        self.nazev = nazev

    def __str__(self) -> str:
        return f"#{self.id}: {self.nazev}"
    def __repr__(self) -> str:
        return self.__str__()

class Sponzor():
    """ Kazdy sponzor ma sve jmeno a list id zvirat, ktera je ochoten sponzorovat"""
    def __init__(self, jmeno: str, zvirata: list[int]) -> None:
        self.jmeno = jmeno
        self.zvirata = zvirata

    def __str__(self) -> str:
        return f"{self.jmeno}: {','.join([str(zvire) for zvire in self.zvirata])}"
    def __repr__(self) -> str:
        return self.__str__()


def vypis_reseni(reseni: list[tuple[Zvire,Sponzor]], hotovo: bool):
    print("Ano" if hotovo else "Ne")
    reseni.sort(key=lambda obj: obj[0].nazev) # serad reseni podle nazvu zvirete
    for par in reseni:
        print(f"{par[0].nazev} {par[1].jmeno}") # vypis nazev zvirete a jmeno sponzora

    # log.info(f'Operations: {operations}') # TODO: remove

operations = 0
iterace = 1
nejlepsi_reseni = []
def solve(zvirata: list[Zvire], sponzori: list[Sponzor], reseni = [], depth = 0) -> tuple[list[tuple[Zvire,Sponzor]],bool]:
    """ Funkce prijima list vsech zvirat, list sponzoru, kteri jeste zadne zvire nesponzoruji (rekurzivni funkce jeste prijima soucasne nekompletni reseni)
    vraci par hodnot - list reseni(paru zvirat a sponzoru) a hodnotu pravda/neprava jestli je toto reseni kompletni a platne"""
    global iterace, nejlepsi_reseni, operations


    hotovo = False
    while not hotovo:
        pad = '  '*depth
        log.debug(f"\n {pad}iterace #{iterace} {depth=}")


        # nejdrive zkusime najit sponzora, ktery ma jen jedno zvire
        nalezen_sponzor = False
        for sponzor in sponzori:
            operations += 1
            if len(sponzor.zvirata) == 1:
                id = sponzor.zvirata[0]
                reseni.append( (zvirata[id],sponzor) )
                # sponzor.sponzoruje = True
                sponzori.remove(sponzor)

                # odeber id od ostatnich sponzoru, protoze je uz zabrane
                for s in sponzori:
                    if id in s.zvirata:
                        s.zvirata.remove(id)
                nalezen_sponzor = True

                if len(reseni) == len(zvirata): # porovnej jestli uz kazde zvire ma sveho sponzora = delka reseni se rovna poctu zvirat
                    hotovo = True
                    log.debug(f'{pad}[+] done -> len(reseni) == len(zvirata)')
                    return (reseni,True)


        # pokud dosli sponzori uz nema smysl resit dal - soucasne reseni je nejlepsi
        if len(sponzori) == 0:
            vypis_reseni(reseni, False)
            exit()



        if not nalezen_sponzor:
            log.debug(f"{pad}[-] Nenalezen sponzor -> rekurze")

            # projdi postupne vsechny sponzory, kteri jeste nesponzoruji
            # zacni od tech, kteri maji nejmene zvirat - jsou nejproblematictejsi
            sponzori.sort(key=lambda obj: len(obj.zvirata)) # serad sponzory podle poctu zvirat
            for i,sponzor in enumerate(sponzori):
                operations += 1
                if len(sponzor.zvirata) == 0: # ignoruje sponzory, kteri uz nemaji zadna zvirata
                    continue
                log.debug(f"{pad}{sponzor.jmeno}")
                for zvire in sponzor.zvirata:
                    log.debug(f"Trying ({sponzor} #{i}) + {zvire}")

                    sponzori_2 = deepcopy(sponzori) # vytvoreni kopii listu aby je rekurzivni funkce neprepsala
                    sponzori_2.pop(i) # odeber sponzora z listu sponzoru
                    reseni_2 = deepcopy(reseni)


                    # odeber id od ostatnich sponzoru, protoze dane zvire uz je sponzorovane
                    for s in sponzori_2:
                        if zvire in s.zvirata:
                            s.zvirata.remove(zvire)

                    reseni_2.append( (zvirata[zvire],sponzor) ) # pridej zvire + sponzora do seznamu reseni

                    # for s in enumerate(sponzori):
                        # log.debug(f"{pad}S1: {s}")

                    nove_reseni, nove_hotovo = solve(zvirata, sponzori_2, reseni_2, depth+1) # pokracuj rekurzi

                    if nove_hotovo: # pokud bylo v rekurzi nalezeno reseni, tak ho predej vys
                        log.debug(f'{pad}[+] rekurze vyresila problem')
                        return (nove_reseni,True)
            
            log.debug(f"{pad}[-] toto odvetvi nema reseni")

            # porovnej jestli toto (nekompletni) reseni je lepsi nez predchozi nejlepsi reseni
            if len(reseni) > len(nejlepsi_reseni):
                nejlepsi_reseni = deepcopy(reseni) # pokud ano, tak nastav toto reseni jako nejlepsi

            return (reseni, False) # vrat False - nebylo nalezeno reseni - a vrat soucasne nekompletni reseni
                


        iterace += 1
        # for zvire in zvirata:
            # log.debug(f"{pad}Z: {zvire}")
        # for sponzor in sponzori:
            # log.debug(f"{pad}S: {sponzor}")
        # for par in reseni:
            # log.debug(f"{pad}R: {par}")

    return (reseni,False)


def main():

    # Vstup
    # Na prvnim radku se nachazi 2 cela cisla: pocet zvirat 1 <= N <= 100
    # a pocet jejich potencialnich sponzoru 1 <= M <= 200.
    N, M = (int(i) for i in input().split(' '))

    zvirata: list[Zvire] = [None]*N
    sponzori: list[Sponzor] = [None]*M

    # Nasleduje N radku s informacemi o jednotlivych zviratech:
    # unikatni celociselne id n-teho zvirete 0 <= In < N
    # a jeho jednoslovny nazev. Nazvy zvirat jsou take unikatni.
    for _ in range(N):
        line = input().split(' ')
        id = int(line[0])
        nazev = line[1]
        zvirata[id] = Zvire(id, nazev)

    # Za nimi se nachazi M radku s informacemi o potencialnich sponzorech.
    # Kazdy radek zacina krestnim jmenem m-teho sponzora
    # a poctem zvirat 1 <= Pm <= N , z kterych je jedno ochoten sponzorovat.
    # Nasleduje Pm id techto zvirat. Krestni jmeno kazdeho sponzora je vzdy unikatni.
    for m in range(M):
        line = input().split(' ')
        jmeno = line[0]
        pocet = int(line[1])

        sponzorovana_zvirata: list[int] = []
        
        for p in range(2, 2+pocet): # nacti vsechna id zvirat, ktera je ochoten sponzorovat
            id = int(line[p])
            sponzorovana_zvirata.append(id)

        sponzori[m] = Sponzor(jmeno, sponzorovana_zvirata)

    # for zvire in zvirata:
        # log.debug(str(zvire))
    # for sponzor in sponzori:
        # log.debug(str(sponzor))
    # log.debug('-'*20)


    # Vystup
    # Na prvnim radku vypis Ano, pokud jde vsem zviratum najit sponzora, Ne v opacnem pripade.
    # Pro vsechna zvirata, kterym jsi nasel sponzora, vypis na samostatny radek nazev zvirete
    # a jmeno jeho sponzora oddelene mezerou.
    # Radky serad vzestupne dle abecedy podle nazvu zvirete.
    # Uloha muze mit vice spravnych reseni, vypis libovolne z nich.

    reseni, hotovo = solve(zvirata, sponzori)

    # Vypis reseni
        # pokud je reseni kompletni a platne, tak vypis "Ano" a na kazdy radek vypis jeden par z reseni
        # pokud reseni neni kompletni, tak vypis "Ne" a na kazdy radek vypis jeden par z nejlepsiho nalezeneho reseni
    vypis_reseni(reseni if hotovo else nejlepsi_reseni, hotovo)


if __name__ == "__main__":
    main()
