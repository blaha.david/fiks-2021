#include <stdio.h>
#include <vector>
#include <algorithm>
#include <string.h>

#define MAX_STR_LEN 51 // maximalni delka jmena je 50 znaku + null byte

// navratove hodnoty funkce solve
#define MA_RESENI 1
#define NEMA_RESENI -1
#define ZATIM_NEVYRESENO 0

struct Sponzor {
    int jmenoIndex;
    std::vector<int> zvirata;
    Sponzor(){}
    Sponzor(int _jmenoIndex, std::vector<int> _zvirata) : jmenoIndex(_jmenoIndex), zvirata(_zvirata) {}
};

// Par nazvu zvirete a jmena sponzora do reseni
struct Par {
    int zvireIndex;
    int sponzorIndex;
    Par(int _zvireIndex, int _sponzorIndex) : zvireIndex(_zvireIndex), sponzorIndex(_sponzorIndex) {}
};


// globalni promenne na ukladani jmen zvirat a sponzoru (stringu)
char** jmenaZvirat;
char** jmenaSponzoru;


// pouzito na serazeni reseni
bool porovnejNazevZvirete (Par a, Par b) {
    return strcmp(jmenaZvirat[a.zvireIndex],jmenaZvirat[b.zvireIndex]) < 0;
}

// pouzito na serazeni sponzoru kvuli optimalizace algoritmu
bool porovnejPocetZviratSponzora (Sponzor a, Sponzor b) {
    return a.zvirata.size() < b.zvirata.size();
}

// Vypis reseni
// pokud je reseni kompletni a platne, tak vypis "Ano" a na kazdy radek vypis jeden par z reseni
// pokud reseni neni kompletni, tak vypis "Ne" a na kazdy radek vypis jeden par z nejlepsiho nalezeneho reseni
void vypisReseni (std::vector<Par> &reseni, int vysledek) {
    if (vysledek == MA_RESENI) {
        printf("Ano\n");
    } else {
        printf("Ne\n");
    }

    // serad reseni podle nazvu zvirete
    sort(reseni.begin(), reseni.end(), porovnejNazevZvirete);

    for (std::vector<Par>::iterator par = reseni.begin(); par != reseni.end(); par++) {
        printf("%s %s\n", jmenaZvirat[par->zvireIndex], jmenaSponzoru[par->sponzorIndex]);
    }
}


int solve(const size_t lenZvirat, const size_t N, std::vector<Sponzor> sponzori, std::vector<Par> reseni, std::vector<Par> &nejlepsiReseni, int depth) {

    while (true) {
loop:

        bool nalezenSponzor = false;

        // nejdrive zkusime najit sponzora, ktery ma jen jedno zvire
        // projdeme vsechy sponzory
        for (std::vector<Sponzor>::iterator sponzor = sponzori.begin(); sponzor != sponzori.end();) { // foreach sponzor

            // a porovej jestli ma jen jedno zvire
            if (sponzor->zvirata.size() == 1) {
                int id = sponzor->zvirata[0];

                reseni.emplace_back(id, sponzor->jmenoIndex);

                // porovnej jestli uz kazde zvire ma sveho sponzora = delka reseni se rovna poctu zvirat
                if (reseni.size() == lenZvirat) {

                    nejlepsiReseni = reseni;
                    return MA_RESENI;
                }

                // projdi vsechny ostatni sponzory a odeber od nich id zvirete
                for (size_t os = 0;os < sponzori.size();os++) {
                    for (std::vector<int>::iterator zvire = sponzori[os].zvirata.begin(); zvire != sponzori[os].zvirata.end(); zvire++) { // projdi vsechny jeho sponzorovana zvirata
                        if (*zvire == id) {
                            sponzori[os].zvirata.erase(zvire);
                            break;
                        }
                    }
                }

                nalezenSponzor = true;

                sponzor = sponzori.erase(sponzor);
            } else {
                sponzor++;
            }
        }


        // Pokud pocet sponzoru, kteri jeste nesponzoruji, je roven nule, znamena to, ze vstup nema reseni a aktualni nekompletni reseni je nejlepsi
        if (sponzori.size() == 0) {
            nejlepsiReseni = reseni;
            return NEMA_RESENI;
        }




        // pokud byl nalezen sponzor, tak pokracuj ve smycce, pokud ne, tak zacni v rekurzi zkouset postupne pridelovat vsechna zvirata sponzorum
        if (!nalezenSponzor) {

            // zkus najit zvire, ktere je sponzorovano jenom jednim sponzorem
            int *pocet = new int[N]; // pocet sponzoru, kteri sponzoruji toot zvire (index je id zvirete)
            int *jedinySponzor = new int[N]; // id jedineho sponzora, ktery toto zvire sponzoruje (index je id zvirete), pokud je jich vice hodnota je -1;

            for (size_t i = 0; i < N; i++) {
                pocet[i] = 0;
                jedinySponzor[i] = -1;
            }

            for (size_t i = 0;i < sponzori.size(); i++) {
                for (std::vector<int>::iterator zvire = sponzori[i].zvirata.begin(); zvire != sponzori[i].zvirata.end(); zvire++) {
                    pocet[*zvire]++;
                    if (pocet[*zvire] == 1) {
                        jedinySponzor[*zvire] = i;
                    } else {
                        jedinySponzor[*zvire] = -1;
                    }
                }
            }
            for (int id = 0;id < (int)N; id++) {
                if (pocet[id] == 1) {
                    int indexSponzora = jedinySponzor[id];

                    reseni.emplace_back(id, sponzori[indexSponzora].jmenoIndex);

                    // porovnej jestli uz kazde zvire ma sveho sponzora = delka reseni se rovna poctu zvirat
                    if (reseni.size() == lenZvirat) {
                        nejlepsiReseni = reseni;
                        return MA_RESENI;
                    }

                    sponzori.erase(sponzori.begin()+indexSponzora);

                    // projdi vsechny ostatni sponzory a odeber od nich id zvirete
                    for (std::vector<Sponzor>::iterator sponzor = sponzori.begin(); sponzor != sponzori.end(); sponzor++) {
                        for (std::vector<int>::iterator zvire = sponzor->zvirata.begin(); zvire != sponzor->zvirata.end(); zvire++) { // projdi vsechny jeho sponzorovana zvirata
                            if (*zvire == id) {
                                sponzor->zvirata.erase(zvire);
                                break;
                            }
                        }
                    }
                    delete[] pocet;
                    delete[] jedinySponzor;
                    goto loop;
                }
            }
            delete[] pocet;
            delete[] jedinySponzor;

            // projdi postupne vsechny sponzory, kteri jeste nesponzoruji, a jejich zvirata
            // zacni od tech, kteri maji nejmene zvirat - jsou nejproblematictejsi
            sort(sponzori.begin(), sponzori.end(), porovnejPocetZviratSponzora);// serad sponzory podle poctu zvirat
            for (std::vector<Sponzor>::iterator sponzor = sponzori.begin(); sponzor != sponzori.end(); sponzor++) {

                if (sponzor->zvirata.size() == 0) { // ignoruj sponzory, kteri nemaji zadna zvirata
                    continue;
                }

                // Projdi vsechny zvirata tohohle sponzora
                for (size_t z = 0;z < sponzor->zvirata.size();z++) {

                    std::vector<Par> reseni_2 = reseni; //vytvoreni kopie vectoru aby ho rekurzivni funkce "nezkazila"

                    //vytvoreni kopie vectoru aby ho rekurzivni funkce "nezkazila"
                    //do noveho vectoru se neprida tento sponzor
                    std::vector<Sponzor> sponzori_2;
                    sponzori_2.reserve(sponzori.size());
                    for (size_t i = 0;i < sponzori.size();i++) {
                        if (sponzori[i].jmenoIndex != sponzor->jmenoIndex && sponzor->zvirata.size() != 0) { // je zbytecne aby se kopirovat sponzor, ktery uz nema zadna zvirata
                            sponzori_2.emplace_back(sponzori[i].jmenoIndex, sponzori[i].zvirata);
                        }
                    }


                    // odeber id od ostatnich sponzoru, protoze dane zvire uz je sponzorovane
                    for (size_t os = 0;os < sponzori_2.size();os++) {
                        for (std::vector<int>::iterator zvire = sponzori_2[os].zvirata.begin(); zvire != sponzori_2[os].zvirata.end(); zvire++) {
                            if (*zvire == sponzor->zvirata[z]) {
                                sponzori_2[os].zvirata.erase(zvire);
                                break;
                            }
                        }
                    }

                    // pridej zvire + sponzora do reseni
                    reseni_2.emplace_back(sponzor->zvirata[z], sponzor->jmenoIndex);

                    // porovnej jestli uz kazde zvire ma sveho sponzora = delka reseni se rovna poctu zvirat
                    if (reseni_2.size() == lenZvirat) {
                        nejlepsiReseni = reseni_2;
                        return MA_RESENI;
                    }

                    // zkus jestli situaci jde vyresit v rekurzi
                    int result = solve(lenZvirat, N, sponzori_2, reseni_2, nejlepsiReseni, depth+1);

                    if (result != ZATIM_NEVYRESENO) { // pokud je vysledek 1(nalezeno reseni) anebo -1(nema reseni), tak eskaluj z rekurze
                        return result;
                    }
                    // jinak pokracuj ve smycce
                }
            }

            // porovnej jestli toto (nekompletni) reseni je lepsi nez predchozi nejlepsi reseni
            if (reseni.size() > nejlepsiReseni.size()) {
                // pokud ano, tak nastav toto reseni jako nejlepsi
                nejlepsiReseni = reseni;
            }

            return ZATIM_NEVYRESENO; // pokracuj ve smycce o patro vys
        }
    }

    return ZATIM_NEVYRESENO;
}


int main () {

    // Vstup
    // Na prvnim radku se nachazi 2 cela cisla: pocet zvirat 1 <= N <= 100
    // a pocet jejich potencialnich sponzoru 1 <= M <= 200.
    int N, M;

    scanf("%d %d", &N, &M);

    jmenaZvirat = new char*[N];
    jmenaSponzoru = new char*[M];


    // Nasleduje N radku s informacemi o jednotlivych zviratech:
    // unikatni celociselne id n-teho zvirete 0 <= In < N
    // a jeho jednoslovny nazev. Nazvy zvirat jsou take unikatni.
    for (int n = 0; n < N; n++) {
        int id;
        scanf("%d", &id);

        jmenaZvirat[id] = new char[MAX_STR_LEN];
        scanf("%50s", jmenaZvirat[id]);
    }


    // Za nimi se nachazi M radku s informacemi o potencialnich sponzorech.
    // Kazdy radek zacina krestnim jmenem m-teho sponzora
    // a poctem zvirat 1 <= Pm <= N , z kterych je jedno ochoten sponzorovat.
    // Nasleduje Pm id techto zvirat. Krestni jmeno kazdeho sponzora je vzdy unikatni.
    std::vector<Sponzor> sponzori(M);

    bool *jeZvireSponzorovane = new bool[N];

    for (int m = 0; m < M; m++) {
        jmenaSponzoru[m] = new char[MAX_STR_LEN];
        scanf("%50s", jmenaSponzoru[m]);

        sponzori[m].jmenoIndex = m;

        int Pm;
        scanf("%d", &Pm);
        sponzori[m].zvirata.reserve(Pm);

        for (int i = 0;i < Pm; i++) {
            int szId; // id sponzorovaneho zvirete
            scanf("%d", &szId);
            sponzori[m].zvirata.emplace_back(szId);
            jeZvireSponzorovane[szId] = true;
        }
    }

    // Vystup
    // Na prvnim radku vypis Ano, pokud jde vsem zviratum najit sponzora, Ne v opacnem pripade.
    // Pro vsechna zvirata, kterym jsi nasel sponzora, vypis na samostatny radek nazev zvirete
    // a jmeno jeho sponzora oddelene mezerou.
    // Radky serad vzestupne dle abecedy podle nazvu zvirete.
    // Uloha muze mit vice spravnych reseni, vypis libovolne z nich.

    std::vector<Par> reseni;
    std::vector<Par> nejlepsiReseni; // pass by reference

    // spocitej pocet sponzorovatelnych zvirat (ignoruj zvirata, ktera nikdo neni ochoten sponzorovat)
    int pocetZvirat = 0;
    for (int id = 0;id < N; id++) {
        if (jeZvireSponzorovane[id]) {
            pocetZvirat++;
        }
    }
    delete[] jeZvireSponzorovane;

    int vysledek = solve(pocetZvirat, N, sponzori, reseni, nejlepsiReseni, 1);

    if (pocetZvirat != N) {
        vysledek = NEMA_RESENI;
    }

    vypisReseni(nejlepsiReseni, vysledek);


    // Cleanup
    for (int n = 0;n < N; n++) {
        delete[] jmenaZvirat[n];
    }
    delete[] jmenaZvirat;
    for (int m = 0;m < M; m++) {
        delete[] jmenaSponzoru[m];
    }
    delete[] jmenaSponzoru;
}
