#! /usr/bin/env python3

from subprocess import Popen, PIPE
import time

solver = './solve'
generator = './generator.py'

REPEAT = 10

def test(z, s, zps_min, zps_max):

    # generate input
    with Popen([generator, str(z), str(s), str(zps_min), str(zps_max)], stdout=PIPE) as gen:
        vstup = gen.stdout.read()

    # print("Generated input: ")
    # print(vstup.decode())

    begin_time = time.time()

    for _ in range(REPEAT):
        with Popen(solver, stdin=PIPE, stdout=PIPE) as solve:

            # output = solve.communicate(input=vstup)[0].decode()
            # print(output)
            solve.communicate(input=vstup)

    end_time = time.time()

    return (end_time - begin_time) * 1000




def main():
    for z in range(10,20):
        for s in range(10,20):
            diff_ms = test(z,s,1,z)
            print(f"Test: {z} {s} {1} {z} --> ", end='')
            print(f"{diff_ms:.03f} ms = {diff_ms/REPEAT:.03f} ms/execution")




if __name__ == '__main__':
    main()
