# Sponzoři


## Popis algoritmu

Algoritmus nejdříve projede všechny sponzory, kteří ještě nikoho nesponzorují, a pokud mu ze zvířat, která je ochoten sponzorovat, zbývá pouze jedno (buď protože je ochoten sponzorovat pouze jedno, anebo protože všechny ostatní již sponzoruje někdo jiný), tak do řešení přidá tento pár (zvíře + sponzor), a u všech ostatních sponzorů odstraní toto zvíře ze seznamu zvířat, která sponzorují

Pokud touto metodou přidělil nového sponzora, tak tuto metodu opakuje.

Pokud je počet párů v řešení roven počtu zvířat (každé zvíře má sponzora), tak vrať, že vstup má řešení a nastav nejlepší řešení na aktuální řešení.

Pokud počet sponzorů, kteří ještě nesponzorují, je roven nule, znamená to, že vstup nemá řešení a aktuální nekompletní řešení je nejlepší (co nejvíce zvířat má sponzora)

Pokud nebyl nalezen sponzor pro nějaké zvíře, tak se pokusí najít zvíře, které má pouze jednoho sponzora a přiřat mu ho.

Pokud nebyl přidělen žádný nový sponzor (žádnému sponzorovi nezbývá právě jedno zvíře a žádné zvíře nemá právě jednoho sponzora), tak projde postupně všechny sponzory a všechna jejich zvířata a vždy zkusí tento jeden pár přidělit (a opět od ostatních sponzorů odstanit toto zvíře) a zkusit situaci dořešit v rekurzi(celý funkce algoritmu se opakuje, akorát pouze se zbývajícími sponzory).
Pokud rekurze situaci nevyřeší, tak smyčka pokračuje a zkusí se jiné zvíře od tohoto sponzora.
A poté se zkusí jiný sponzor.

Algoritmus lze optimalizovat seřazením sponzorů vzestupně podle počtu zvířat.
Sponzoři, kterí mají menší počet zvířat jsou problematičtější, protože když je někdo "předběhne", tak už nemohou žádné zvíře sponzorovat.
A proto je lepší, když se nejdříve přiřadí oni, a až poté sponoři s větším počtem zvířat, protože je pravděpodobnější, že jim nějaká zvířata na sponzorování zbydou.

Algoritmus skončí pokud byl všem zvířatům přidělen sponzor (délka řešení se rovná počtu zvířat) anebo pokud již nezbývají žádní sponzoři - tehdy je aktuální řešení nejlepší možné, přestože není kompletní (např. pokud je počet sponzorů menší než počet zvířat).
Pokud rekurze nenajde žádné řešení, tak také končí a vypíše doposud nejlepší(nejdelší) řešení.


## Správnost algoritmu

Algoritmus je správný, protože v nejhorším případě "bruteforcuje" každého sponzora s každým jeho zvířetem, a tak garantuje, že bude nalezeno nejlepší možné řešení.


## Odhad asymptotické složitosti

Použité funkce z knihoven:
- **sort** : `O(n log(n))`
- **vector**:
  - erase: `O(n)`
  - emplace\_back: `O(1)`
- **strcmp**: `O(n)`


Asymptotická složitost záleží na počtu sponzorů a počtu zvířat.

Záleží taky na rozdílu počtu sponzorů a zvířat:
  - pokud je 20 zvířat a 20 sponzorů, tak je složitější najít každému zvířeti toho správného sponzora
  - pokud je 20 zvířat a 40 sponzorů, je to mnohem snazší

Zároveň ale taky hodně záleží na tom, kolik zvířat jsou sponzoři ochotni sponzorovat, což není jednotná vstupní hodnota
  - pokud pokud je 20 zvířat a každý z 20 sponzorů sponzoruje jedno zvíře jiné než ostatní, je složitost 20
  - pokud každý sponzoruje např. 15, bude složitost mnohem větší
  - naopak zase pokud každý sponzor sponzoruje všech 20 zvířat, je složitost opět 20

tak nevím jak to zapsat.

V nejhorším případě musí algoritmus projít rekurzivně všechny zvířata všech sponzorů.
Ale jsou tam optimalizace, které to zlepšují.
