
# Bezpečnostní kamery

## Část 1

Každá kamera vezme svůj unikátní identifikátor a rozdělí jej na prvočinitele.
Svoji frekvenci nastaví na počet těchto prvočinitelů.
Například KAM2 bude mít frekvenci 1, KAM6 bude mít frekvenci 2 a KAM8 bude mít frekvenci 3.

Kamery jsou blízko sebe pokud jsou propojeny kabelem.
Pokud jsou propojeny kabelem, znamená to, že id druhé kamery je násobkem id první kamery.
To znamená, že druhá kamera má (minimálně) o jednoho prvočinitele víc než první kamera.
To znamená, že žádné dvě kamery, které jsou propojeny, nebudou mít stejný počet prvočinitelů
a tím pádem nebudou mít stejnou frekvenci a nebudou se rušit.

Na kamery s id od 2 do 15 (14 kamer) nám stačí tři frekvence.
Na kamery s id od 2 do n nám stačí `floor(log2(n))` frekvencí.


## Část 2

Napadlo mě řešení, kdy každá kamera svojí frekvenci určí podle počtu vstupních kabelů.

nemá žádný vstup --> f1
má jeden vstup   --> f2
má dva vstupy    --> f3
má tři vstupy    --> f4

Opět platí, že pokud jsou spojeny, tak ta druhá bude mít (minimálně) o jedno spojení víc a tudíš vyšší frekvenci a nebudou se proto rušit.

Ale není to optimální - pro 14 kamer potřebujeme 5 frekvencí místo 3 - například KAM6 by měla f3, ale stačila by f2, a KAM12 f5 místo f3.




Pak mě napadlo, že by každá kamera poslala jedničku, pokud je její frekvence lichá a nulu, pokud je sudá.
A pokud kamera přijme sudý počet jedniček, tak nastaví svoji frekvenci o jedna menší (pokud není 1).
(Každá kamera by vždy počkala, než dostane bity ze vstupů a pak až si nastaví svoji frekvenci a pošle ji na své výstupy)

Touto metodou by KAM6 měla f2 a KAM12 f4, což také není nejoptimálnější, ale je to lepší než předtím.

