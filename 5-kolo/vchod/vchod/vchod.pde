
String[] lines;

//String[] lines = new String[]{
//  "10 6",
//  "+ -4 3",
//  "+ 7 14",
//  "+ 10 -3",
//  "? 4 0",
//  "- 7 14",
//  "? 8 0"
//};
//String[] lines = new String[]{
//  "10 9",
//  "+ -4 3",
//  "+ 7 14",
//  "+ 10 -3",
//  "+ -4 -3",
//  "+ 14 2",
//  "? 4 0",
//  "? 0 4",
//  "? 10 4",
//  "? 4 10"
//};

int index = 1;

int N, Q;

int minI, minJ, maxI, maxJ;

int doorI = Integer.MAX_VALUE, doorJ = Integer.MAX_VALUE;

ArrayList<House> houses = new ArrayList<House>();

PrintWriter writer;

void setup () {
  size(800, 800);

  lines = loadStrings("input.txt");

  writer = createWriter("output.txt");

  String[] NQ = lines[0].split(" ");
  N = Integer.parseInt(NQ[0]);
  Q = Integer.parseInt(NQ[1]);

  minI = 0;
  minJ = 0;
  maxI = N;
  maxJ = N;

  while (index < 30000) {
    next();
    if (index % 1000 == 0) {
      println(index);
      writer.flush();
    }
  }
  writer.flush();

  noLoop();
  rectMode(CENTER);
  ellipseMode(CENTER);
  
  exit();
}


void draw () {
  background(50);

  //float tileSize = min(width/(maxI-minI+1), height/(maxJ-minJ+1));

  //pushMatrix();
  //translate(-(minI-0.5f)*tileSize, -(minJ-0.5f)*tileSize);

  //// Draw ZOO walls:
  //for (int i = 0; i <= N; i++) {
  //  stroke(255, 0, 0);
  //  strokeWeight(5);
  //  noFill();
  //  rect(i*tileSize, 0, tileSize/2, tileSize/2);
  //  rect(0, i*tileSize, tileSize/2, tileSize/2);
  //  rect(i*tileSize, N*tileSize, tileSize/2, tileSize/2);
  //  rect(N*tileSize, i*tileSize, tileSize/2, tileSize/2);
  //}


  //// Draw houses:
  //for (House house : houses) {
  //  stroke(0, 255, 0);
  //  rect(house.i*tileSize, house.j*tileSize, tileSize/2, tileSize/2);
  //}

  //// Draw door:
  //if (doorI != Integer.MAX_VALUE) {
  //  stroke(0, 255, 255);
  //  rect(doorI*tileSize, doorJ*tileSize, tileSize, tileSize);


  //  for (House house : houses) {
  //    PVector housePos = new PVector(house.i, house.j);

  //    PVector prev = housePos;
  //    for (PVector point : house.path) {

  //      stroke(255, 200);
  //      strokeWeight(2);
  //      line(prev, point, tileSize);
  //      prev = point;
  //    }
  //    if (house.alternatePath != null) {
  //      prev = housePos;
  //      for (PVector point : house.alternatePath) {

  //        stroke(255, 255, 0, 200);
  //        strokeWeight(2);
  //        line(prev, point, tileSize);
  //        prev = point;
  //      }
  //    }
  //  }
  //}

  //popMatrix();
}


void next () {
  String[] input = lines[index].split(" ");
  String operator = input[0];
  int i = Integer.parseInt(input[1]);
  int j = Integer.parseInt(input[2]);

  doorI = Integer.MAX_VALUE;
  doorJ = Integer.MAX_VALUE;

  //println(lines[index]);

  if (operator.equals("+")) {
    //println("  Adding new house");

    houses.add(new House(i, j));

    if (i < minI) {
      minI = i;
    }
    if (i > maxI) {
      maxI = i;
    }
    if (j < minJ) {
      minJ = j;
    }
    if (j > maxJ) {
      maxJ = j;
    }
  } else if (operator.equals("-")) {
    //println("  Removing house");
    for (int index = houses.size()-1; index >= 0; index--) { // TODO: hashmap??
      House h = houses.get(index);
      if (h.i == i && h.j == j) {
        houses.remove(index);
      }
    }
  } else if (operator.equals("?")) {
    //println("  Query");
    doorI = i;
    doorJ = j;
    query();
    calculateDistances();
  } else {
    println("  Invalid input!");
  }

  index++;
  redraw();
}


void query () {
  for (House house : houses) {

    PVector housePos = new PVector(house.i, house.j);
    PVector doorPos = new PVector(doorI, doorJ);

    house.path.clear();
    house.alternatePath = null;

    house.path.add(housePos);


    // DOOR LEFT:
    if (doorI == 0) {
      if (house.i < 0) {
        // house left
        if (house.j < N/2f) {
          house.path.add(new PVector(0, 0));
        } else {
          house.path.add(new PVector(0, N));
        }
      } else if (house.j < 0) {
        // house top
        house.path.add(new PVector(0, 0));
      } else if (house.j > N) {
        // house bottom
        house.path.add(new PVector(0, N));
      } else if (house.i > N) {
        // house right
        house.path.add(new PVector(N, 0)); // top path
        house.path.add(new PVector(0, 0));

        house.alternatePath = new ArrayList<PVector>(); // bottom path
        house.alternatePath.add(housePos);
        house.alternatePath.add(new PVector(N, N));
        house.alternatePath.add(new PVector(0, N));
        house.alternatePath.add(doorPos);
      }
    }
    // DOOR TOP:
    else if (doorJ == 0) {
      if (house.j < 0) {
        // house top
        if (house.i < N/2f) {
          house.path.add(new PVector(0, 0));
        } else {
          house.path.add(new PVector(N, 0));
        }
      } else if (house.i < 0) {
        // house left
        house.path.add(new PVector(0, 0));
      } else if (house.i > N) {
        // house right
        house.path.add(new PVector(N, 0));
      } else if (house.j > N) {
        // house bottom
        house.path.add(new PVector(0, N)); // left path
        house.path.add(new PVector(0, 0));

        house.alternatePath = new ArrayList<PVector>(); // right path
        house.alternatePath.add(housePos);
        house.alternatePath.add(new PVector(N, N));
        house.alternatePath.add(new PVector(N, 0));
        house.alternatePath.add(doorPos);
      }
    }
    // DOOR RIGHT:
    else if (doorI == N) {
      if (house.i > N) {
        // house right
        if (house.j < N/2f) {
          house.path.add(new PVector(N, 0));
        } else {
          house.path.add(new PVector(N, N));
        }
      } else if (house.j < 0) {
        // house top
        house.path.add(new PVector(N, 0));
      } else if (house.j > N) {
        // house bottom
        house.path.add(new PVector(N, N));
      } else if (house.i < 0) {
        // house left
        house.path.add(new PVector(0, 0)); // top path
        house.path.add(new PVector(N, 0));

        house.alternatePath = new ArrayList<PVector>(); // bottom path
        house.alternatePath.add(housePos);
        house.alternatePath.add(new PVector(0, N));
        house.alternatePath.add(new PVector(N, N));
        house.alternatePath.add(doorPos);
      }
    }
    // DOOR BOTTOM:
    else if (doorJ == N) {
      if (house.j > N) {
        // house bottom
        if (house.i < N/2f) {
          house.path.add(new PVector(0, N));
        } else {
          house.path.add(new PVector(N, N));
        }
      } else if (house.i < 0) {
        // house left
        house.path.add(new PVector(0, N));
      } else if (house.i > N) {
        // house right
        house.path.add(new PVector(N, N));
      } else if (house.j < 0) {
        // house top
        house.path.add(new PVector(0, 0)); // left path
        house.path.add(new PVector(0, N));

        house.alternatePath = new ArrayList<PVector>(); // right path
        house.alternatePath.add(housePos);
        house.alternatePath.add(new PVector(N, 0));
        house.alternatePath.add(new PVector(N, N));
        house.alternatePath.add(doorPos);
      }
    } else {
      println("Něco jsi posral:tm:");
    }
    house.path.add(doorPos);
  }
}

void calculateDistances () {
  float max = 0f;
  for (House house : houses) {
    float len = house.getLen();
    if (len > max) {
      max = len;
    }
  }
  writer.println((int)round(max));
}

void keyPressed () {
  if (key == ' ') next();
}

void line (PVector a, PVector b, float tileSize) {
  line(a.x*tileSize, a.y*tileSize, b.x*tileSize, b.y*tileSize);
}
