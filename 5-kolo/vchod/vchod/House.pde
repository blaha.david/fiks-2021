class House {
  int i;
  int j;
  ArrayList<PVector> path;
  ArrayList<PVector> alternatePath;

  public House (int i, int j) {
    this.i = i;
    this.j = j;
    this.path = new ArrayList<PVector>();
  }

  float getLen () {
    float sum = 0;
    PVector prev = new PVector(i, j);
    for (PVector point : path) {
      sum += PVector.sub(point, prev).mag();
      //sum += len(point, prev);
      prev = point;
    }

    if (alternatePath != null) {
      float altsum = 0;
      prev = new PVector(i, j);
      for (PVector point : alternatePath) {
        altsum += PVector.sub(point, prev).mag();
        //altsum += len(point, prev);
        prev = point;
      }

      if (altsum < sum) {
        sum = altsum;
      }
    }

    return sum;
  }
}


float len(PVector a, PVector b) {
  return (a.x-b.x) * (a.x-b.x) + (a.y-b.y) * (a.y-b.y);
}
