﻿using System;
using System.Collections.Generic;

public class Program
{
    string[] lines;

    //string[] lines = new string[]{
    //"10 6",
    //"+ -4 3",
    //"+ 7 14",
    //"+ 10 -3",
    //"? 4 0",
    //"- 7 14",
    //"? 8 0"
    //};
    //string[] lines = new string[]{
    //  "10 9",
    //  "+ -4 3",
    //  "+ 7 14",
    //  "+ 10 -3",
    //  "+ -4 -3",
    //  "+ 14 2",
    //  "? 4 0",
    //  "? 0 4",
    //  "? 10 4",
    //  "? 4 10"
    //};

    int index = 1;
    int N, Q;
    int doorI = int.MaxValue, doorJ = int.MaxValue;
    List<House> houses = new List<House>();

    Vector center = new Vector(50_000_000, 50_000_000);

    long closestFurthestHouse = long.MaxValue;

    //int queryIndex = 1;
    //int targetQuery = 1656;

    public static void Main(string[] args)
    {
        new Program(args[0]);
    }

    public Program(string path)
    {

        lines = File.ReadAllLines(path);

        string[] NQ = lines[0].Split(" ");
        N = int.Parse(NQ[0]);
        Q = int.Parse(NQ[1]);


        //while (index < 80000)
        //{
        //next();
        //}

        while (index < lines.Length)
        {
            next();
            if (index % 10000 == 0)
            {
                Console.Error.WriteLine(index);
            }
        }


        Console.Error.WriteLine(closestFurthestHouse);

    }

    private void next()
    {
        string[] input = lines[index].Split(" ");
        string op = input[0];
        int i = int.Parse(input[1]);
        int j = int.Parse(input[2]);

        doorI = int.MaxValue;
        doorJ = int.MaxValue;

        //Console.WriteLine(lines[index]);

        if (op == "+")
        {
            int dist = (int)Math.Round(Vector.RealDist(new Vector(i, j), center));
            //Console.Error.WriteLine($"  Add house at {i,9},{j,9}; dist= {dist,9}");

            if (dist > 140000000) // ignore close houses
                houses.Add(new House(i, j));
        }
        else if (op == "-")
        {
            //Console.WriteLine("  Removing house");
            for (int index = houses.Count - 1; index >= 0; index--)
            {
                House h = houses[index];
                if (h.i == i && h.j == j)
                {
                    houses.RemoveAt(index);
                }
            }
        }
        else if (op == "?")
        {
            //Console.Error.WriteLine($"  Query with {houses.Count} houses");
            doorI = i;
            doorJ = j;
            query2();
            long maxDist = calculateDistances();
            Console.WriteLine(maxDist);
            //queryIndex++;
        }
        else
        {
            Console.WriteLine("  Invalid input!");
        }

        index++;
    }

    // Find path for each house
    //void query()
    //{
    //foreach (House house in houses)
    //{

    //Vector housePos = new Vector(house.i, house.j);
    //Vector doorPos = new Vector(doorI, doorJ);

    //house.path.Clear();
    //house.alternatePath = null;

    //house.path.Add(housePos);

    //// DOOR LEFT:
    //if (doorI == 0)
    //{
    //if (house.i < 0)
    //{
    //// house left
    //house.path.Add(new Vector(0, 0));

    //house.alternatePath = new List<Vector>(); // bottom path
    //house.alternatePath.Add(housePos);
    //house.alternatePath.Add(new Vector(0, N));
    //house.alternatePath.Add(doorPos);
    ////if (house.j < N / 2f)
    ////{
    ////house.path.Add(new Vector(0, 0));
    ////}
    ////else
    ////{
    ////house.path.Add(new Vector(0, N));
    ////}
    //}
    //else if (house.j < 0)
    //{
    //// house top
    //house.path.Add(new Vector(0, 0));
    //}
    //else if (house.j > N)
    //{
    //// house bottom
    //house.path.Add(new Vector(0, N));
    //}
    //else if (house.i > N)
    //{
    //// house right
    //house.path.Add(new Vector(N, 0)); // top path
    //house.path.Add(new Vector(0, 0));

    //house.alternatePath = new List<Vector>(); // bottom path
    //house.alternatePath.Add(housePos);
    //house.alternatePath.Add(new Vector(N, N));
    //house.alternatePath.Add(new Vector(0, N));
    //house.alternatePath.Add(doorPos);
    //}
    //}
    //// DOOR TOP:
    //else if (doorJ == 0)
    //{
    //if (house.j < 0)
    //{
    //// house top
    //house.path.Add(new Vector(0, 0));
    //house.alternatePath = new List<Vector>(); // bottom path
    //house.alternatePath.Add(housePos);
    //house.alternatePath.Add(new Vector(N, 0));
    //house.alternatePath.Add(doorPos);
    ////if (house.i < N / 2f)
    ////{
    ////house.path.Add(new Vector(0, 0));
    ////}
    ////else
    ////{
    ////house.path.Add(new Vector(N, 0));
    ////}
    //}
    //else if (house.i < 0)
    //{
    //// house left
    //house.path.Add(new Vector(0, 0));
    //}
    //else if (house.i > N)
    //{
    //// house right
    //house.path.Add(new Vector(N, 0));
    //}
    //else if (house.j > N)
    //{
    //// house bottom
    //house.path.Add(new Vector(0, N)); // left path
    //house.path.Add(new Vector(0, 0));

    //house.alternatePath = new List<Vector>(); // right path
    //house.alternatePath.Add(housePos);
    //house.alternatePath.Add(new Vector(N, N));
    //house.alternatePath.Add(new Vector(N, 0));
    //house.alternatePath.Add(doorPos);
    //}
    //}
    //// DOOR RIGHT:
    //else if (doorI == N)
    //{
    //if (house.i > N)
    //{
    //// house right
    //house.path.Add(new Vector(N, 0));
    //house.alternatePath = new List<Vector>(); // bottom path
    //house.alternatePath.Add(housePos);
    //house.alternatePath.Add(new Vector(N, N));
    //house.alternatePath.Add(doorPos);
    ////if (house.j < N / 2f)
    ////{
    ////house.path.Add(new Vector(N, 0));
    ////}
    ////else
    ////{
    ////house.path.Add(new Vector(N, N));
    ////}
    //}
    //else if (house.j < 0)
    //{
    //// house top
    //house.path.Add(new Vector(N, 0));
    //}
    //else if (house.j > N)
    //{
    //// house bottom
    //house.path.Add(new Vector(N, N));
    //}
    //else if (house.i < 0)
    //{
    //// house left
    //house.path.Add(new Vector(0, 0)); // top path
    //house.path.Add(new Vector(N, 0));

    //house.alternatePath = new List<Vector>(); // bottom path
    //house.alternatePath.Add(housePos);
    //house.alternatePath.Add(new Vector(0, N));
    //house.alternatePath.Add(new Vector(N, N));
    //house.alternatePath.Add(doorPos);
    //}
    //}
    //// DOOR BOTTOM:
    //else if (doorJ == N)
    //{
    //if (house.j > N)
    //{
    //// house bottom
    //house.path.Add(new Vector(0, N));
    //house.alternatePath = new List<Vector>(); // bottom path
    //house.alternatePath.Add(housePos);
    //house.alternatePath.Add(new Vector(N, N));
    //house.alternatePath.Add(doorPos);
    ////if (house.i < N / 2f)
    ////{
    ////house.path.Add(new Vector(0, N));
    ////}
    ////else
    ////{
    ////house.path.Add(new Vector(N, N));
    ////}
    //}
    //else if (house.i < 0)
    //{
    //// house left
    //house.path.Add(new Vector(0, N));
    //}
    //else if (house.i > N)
    //{
    //// house right
    //house.path.Add(new Vector(N, N));
    //}
    //else if (house.j < 0)
    //{
    //// house top
    //house.path.Add(new Vector(0, 0)); // left path
    //house.path.Add(new Vector(0, N));

    //house.alternatePath = new List<Vector>(); // right path
    //house.alternatePath.Add(housePos);
    //house.alternatePath.Add(new Vector(N, 0));
    //house.alternatePath.Add(new Vector(N, N));
    //house.alternatePath.Add(doorPos);
    //}
    //}
    //else
    //{
    //Console.WriteLine("Něco jsi posral:tm:");
    //}
    //house.path.Add(doorPos);
    //}
    ////if (queryIndex == targetQuery)
    ////{
    ////Console.WriteLine("tenhle??");
    ////Console.WriteLine($"Vchod je na {doorI,9},{doorJ,9} btw");
    ////}
    //}
    void query2()
    {
        foreach (House house in houses)
        {

            Vector housePos = new Vector(house.i, house.j);
            Vector doorPos = new Vector(doorI, doorJ);

            Vector topLeft = new Vector(0, 0);
            Vector topRight = new Vector(N, 0);
            Vector bottomLeft = new Vector(0, N);
            Vector bottomRight = new Vector(N, N);

            house.len = 0;
            house.altLen = 0;

            // DOOR LEFT:
            if (doorI == 0)
            {
                if (house.i < 0)
                {
                    // house left
                    house.len += Vector.RealDist(housePos, topLeft);
                    house.len += doorJ;

                    house.altLen += Vector.RealDist(housePos, bottomLeft);
                    house.altLen += N-doorJ;

                }
                else if (house.j < 0)
                {
                    // house top
                    house.len += Vector.RealDist(housePos, topLeft);
                    house.len += doorJ;
                }
                else if (house.j > N)
                {
                    // house bottom
                    house.len += Vector.RealDist(housePos, bottomLeft);
                    house.len += N-doorJ;
                }
                else if (house.i > N)
                {
                    // house right
                    house.len += Vector.RealDist(housePos, topRight); // top path
                    house.len += N;
                    house.len += doorJ;

                    house.len += Vector.RealDist(housePos, bottomRight); // bottom path
                    house.len += N;
                    house.len += N-doorJ;
                }
            }
            // DOOR TOP:
            else if (doorJ == 0)
            {
                if (house.j < 0)
                {
                    // house top
                    house.len += Vector.RealDist(housePos, topLeft);
                    house.len += doorI;

                    house.altLen += Vector.RealDist(housePos, topRight);
                    house.altLen += N-doorI;

                }
                else if (house.i < 0)
                {
                    // house left
                    house.len += Vector.RealDist(housePos, topLeft);
                    house.len += doorI;
                }
                else if (house.i > N)
                {
                    // house right
                    house.len += Vector.RealDist(housePos, topRight);
                    house.len += N-doorI;
                }
                else if (house.j > N)
                {
                    // house bottom
                    house.len += Vector.RealDist(housePos, bottomLeft);
                    house.len += N;
                    house.len += doorI;

                    house.altLen += Vector.RealDist(housePos, bottomRight);
                    house.altLen += N;
                    house.altLen += N-doorI;

                }
            }
            // DOOR RIGHT:
            else if (doorI == N)
            {
                if (house.i > N)
                {
                    // house right
                    house.len += Vector.RealDist(housePos, topRight);
                    house.len += doorJ;

                    house.altLen += Vector.RealDist(housePos, bottomRight);
                    house.altLen += N-doorJ;
                }
                else if (house.j < 0)
                {
                    // house top
                    house.len += Vector.RealDist(housePos, topRight);
                    house.len += doorJ;
                }
                else if (house.j > N)
                {
                    // house bottom
                    house.len += Vector.RealDist(housePos, bottomRight);
                    house.len += N-doorJ;
                }
                else if (house.i < 0)
                {
                    // house left
                    house.len += Vector.RealDist(housePos, topLeft);
                    house.len += N;
                    house.len += doorI;

                    house.altLen += Vector.RealDist(housePos, bottomLeft);
                    house.altLen += N;
                    house.altLen += N-doorI;
                }
            }
            // DOOR BOTTOM:
            else if (doorJ == N)
            {
                if (house.j > N)
                {
                    // house bottom
                    house.len += Vector.RealDist(housePos, bottomLeft);
                    house.len += doorI;

                    house.altLen += Vector.RealDist(housePos, bottomRight);
                    house.altLen += N-doorI;
                }
                else if (house.i < 0)
                {
                    // house left
                    house.len += Vector.RealDist(housePos, bottomLeft);
                    house.len += doorI;
                }
                else if (house.i > N)
                {
                    // house right
                    house.altLen += Vector.RealDist(housePos, bottomRight);
                    house.altLen += N-doorI;
                }
                else if (house.j < 0)
                {
                    // house top
                    house.len += Vector.RealDist(housePos, topLeft);
                    house.len += N;
                    house.len += doorI;

                    house.altLen += N;
                    house.altLen += Vector.RealDist(housePos, topRight);
                    house.altLen += N-doorI;
                }
            }
            else
            {
                Console.WriteLine("Něco jsi posral:tm:");
            }
        }
        //if (queryIndex == targetQuery)
        //{
        //Console.WriteLine("tenhle??");
        //Console.WriteLine($"Vchod je na {doorI,9},{doorJ,9} btw");
        //}
    }

    // Sum up paths of all houses and find the furthest one
    long calculateDistances()
    {
        double max = 0;
        //House house = houses[0];
        for (int i = 0; i < houses.Count; i++)
        {
            //double len = houses[i].getRealLen();
            double len = houses[i].Len();

            //if (queryIndex == targetQuery)
            //Console.WriteLine($"H #{i} = {len:0}, at {houses[i].i,9},{houses[i].j,9}");

            if (len > max)
            {
                max = len;
                //house = houses[i];
            }
        }
        long maxDist = (long)Math.Round(max);
        //Console.Error.WriteLine($"Furthest house {house.i,9},{house.j,9}; dist= {(int)Math.Round(Vector.RealDist(new Vector(house.i, house.j), center)),9}");

        if (maxDist < closestFurthestHouse)
        {
            closestFurthestHouse = maxDist;
        }

        return maxDist;
    }
}


public class House
{
    public int i;
    public int j;
    //public List<Vector> path;
    //public List<Vector>? alternatePath;
    public double len;
    public double altLen;

    public House(int i, int j)
    {
        this.i = i;
        this.j = j;
        //this.path = new List<Vector>();
        len = 0;
        altLen = 0;
    }

    public double Len ()
    {
        if (altLen == 0) return len;
        if (altLen < len) return altLen;
        return len;
    }

    //public double getRealLen()
    //{
    //double sum = 0;
    //Vector prev = new Vector(i, j);
    //foreach (Vector point in path)
    //{
    //sum += Vector.RealDist(point, prev);
    //prev = point;
    //}

    //if (alternatePath != null)
    //{
    //double altsum = 0;
    //prev = new Vector(i, j);
    //foreach (Vector point in alternatePath)
    //{
    //altsum += Vector.RealDist(point, prev);
    //prev = point;
    //}

    //if (altsum < sum)
    //{
    //sum = altsum;
    //}
    //}

    //return sum;
    //}
    //public long getLen()
    //{
    //long sum = 0;
    //Vector prev = new Vector(i, j);
    //foreach (Vector point in path)
    //{
    //sum += Vector.Dist(point, prev);
    //prev = point;
    //}

    //if (alternatePath != null)
    //{
    //long altsum = 0;
    //prev = new Vector(i, j);
    //foreach (Vector point in alternatePath)
    //{
    //altsum += Vector.Dist(point, prev);
    //prev = point;
    //}

    //if (altsum < sum)
    //{
    //sum = altsum;
    //}
    //}

    //return sum;
    //}
}




public struct Vector
{
    public int x;
    public int y;

    public Vector(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public static long Dist(Vector a, Vector b)
    {
        long dx = a.x - b.x;
        long dy = a.y - b.y;
        return dx * dx + dy * dy;
    }
    public static double RealDist(Vector a, Vector b)
    {
        double dx = a.x - b.x;
        double dy = a.y - b.y;
        return Math.Sqrt(dx * dx + dy * dy);
    }
}
