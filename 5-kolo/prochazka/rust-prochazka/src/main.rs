use std::io::stdin;

fn input(buffer: &mut String) -> Vec<usize> {
    buffer.clear();
    stdin().read_line(buffer).unwrap();
    buffer.split_whitespace().map(str::parse::<usize>).map(Result::unwrap).collect()
}

#[derive(Clone, Debug)]
struct Node {
    id: usize,
    edges: Vec<usize>,
}

fn main() {
    let mut buffer = String::new();

    let t = input(&mut buffer)[0];

    for _ti in 0..t {
        //println!("\n--Zadání #{}:", _ti);

        let nm = input(&mut buffer);
        let n = nm[0];
        let m = nm[1];

        let mut nodes = Vec::with_capacity(n);

        for ni in 0..n { // nodes
            nodes.push(Node{id: ni, edges: vec![]})
        }
        for _mi in 0..m { // edges
            let uv = input(&mut buffer);
            let u = uv[0];
            let v = uv[1];
            nodes[u].edges.push(v);
            nodes[v].edges.push(u);
        }

        let result = solve(&mut nodes);
        println!("{}", result);
    }
}


fn solve(nodes: &mut Vec<Node>) -> String {
    //println!("Nodes: {:?}", nodes);

    let mut all_even = true;

    let mut odd_nodes = vec![];

    for node in nodes {
        if node.edges.len() % 2 == 1 {
            //println!("Node #{} is odd", node.id);
            odd_nodes.push(node.id);
            all_even = false;
        }
    }

    if all_even {
        return "Ano.".to_string();
    }

    let len = odd_nodes.len()/2;
    let mut new_edges = vec![];

    for ni in (0..odd_nodes.len()).step_by(2) {
        new_edges.push(format!("{} {}", odd_nodes[ni], odd_nodes[ni+1]));
    }

    return format!("Ne.\n{}\n{}", len, new_edges.join("\n"));
}
