
# FIKS 2021

## 5. kolo

### [Procházka](prochazka/) `10/10`
Odpověz Sfinze!
[download pdf](https://fiks.fit.cvut.cz/files/tasks/season8/round5/prochazka.pdf)


### [Bezpečnostsní kamery](bezpecnostni_kamery/) `4/10`
Zamysli se!
[download pdf](https://fiks.fit.cvut.cz/files/tasks/season8/round5/kamery.pdf)


### [Vchod](vchod/) `10/10`
Odpověz Sfinze!
[download pdf](https://fiks.fit.cvut.cz/files/tasks/season8/round5/vchod.pdf)


