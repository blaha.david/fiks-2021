using System;

public class Process
{
    public const uint StackSize = 16;

    public bool Dead { get; private set; }
    public string Reason { get; private set; } = "";
    public int Index { get; private set; }

    public uint[] Stack { get; private set; } = new uint[StackSize];

    public uint Sp { get; private set; }
    public uint Pc { get; set; }

    public bool Teleport {get;set;}

    public Process(int index, uint pc)
    {
        Index = index;
        Dead = false;
        Pc = pc;
        Sp = StackSize;
    }


    public void Push(uint value)
    {
        if (Sp == 0)
            throw new StackOverflow();

        --Sp;

        Stack[Sp] = value;
    }

    public uint Pop()
    {
        if (Sp >= StackSize)
            throw new StackUnderflow();

        uint result = Stack[Sp];

        Stack[Sp] = 0;

        Sp++;

        return result;
    }

    public void Die(string reason)
    {
        Dead = true;
        Reason = reason;
    }
}


public class ProcessException : Exception {}
public class StackOverflow : ProcessException {}
public class StackUnderflow : ProcessException {}
public class IllegalInstruction : ProcessException {}
public class DivisionByZero : ProcessException {}
public class SegmentationFault : ProcessException {}
public class Satan : ProcessException {}
