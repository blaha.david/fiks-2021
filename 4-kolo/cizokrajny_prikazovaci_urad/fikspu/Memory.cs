public class Memory
{
    public const int MEMORY_SIZE = 256;
    private uint[] mem;

    public Memory()
    {
        this.mem = new uint[MEMORY_SIZE];
    }

    public uint Read(Process p, uint address)
    {
        if (address%MEMORY_SIZE == 0)
        {
            return 0; // address 0 always contains 0
        }
        if (address == 666)
        {
            throw new Satan();
        }

        return mem[address % MEMORY_SIZE];
    }

    public void Write(Process p, uint address, uint value)
    {
        if (address == 0)
        {
            return;
        }
        if (address == 666)
        {
            throw new Satan();
        }

        mem[address % MEMORY_SIZE] = value;
    }

    // Used only for spawning processes
    public void Set (long address, uint value)
    {
        mem[address] = value;
    }

    // Used only for printing, not by processes
    public uint Get(uint i)
    {
        return mem[(long)i % MEMORY_SIZE];
    }
}
