#! /usr/bin/env bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

echo "Compiling..."
dotnet build --os linux --configuration Release

for i in {1..5}; do
    echo -e "\nRunning test #$i:"
    ./bin/Release/net6.0/linux-x64/fikspu < ../tests/vstup$i.txt > ../tests/output$i.txt 2>&1
    diff --strip-trailing-cr ../tests/expected$i.txt ../tests/output$i.txt >/dev/null
    if [ $? -eq 0 ]; then
        echo -e "${GREEN}Passed${NC}"
    else
        echo -e "${RED}Failed:${NC}"
        git diff --color-words --minimal --no-index ../tests/expected$i.txt ../tests/output$i.txt | cat
    fi
done
