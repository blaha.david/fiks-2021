using System;
using System.Numerics;

public abstract class Instruction
{
    public static Instruction[] Instructions =
    {
        new Nop(),
        new Pc(),
        new Push(),
        new Pop(),
        new Swap(),
        new Dup(),
        new PushSS(),
        new Load(),
        new Store(),
        new Add(),
        new Sub(),
        new Div(),
        new Pow(),
        new Brz(),
        new Br3(),
        new Br7(),
        new Brge(),
        new Jmp(),
        new ArmedBomb(),
        new Bomb(),
        new TlPort(),
        new Jntar(),
    };

    public static Instruction DecodeInstruction(uint id)
    {
        if (!IsValid(id))
        {
            throw new IllegalInstruction();
        }
        return Instructions[id];
    }

    public static bool IsValid(uint id)
    {
        return id < (uint)Instructions.Length;
    }

    public static void PrintInstructions()
    {
        for (int id = 0; id < Instructions.Length; id++)
        {
            Console.WriteLine($"0x{id:x}: {Instructions[id].GetType()}");
        }
    }



    public Instruction()
    {
    }

    public abstract void Execute(Process p, Memory mem, uint param);
}

/// <summary>
/// žádná operace
/// </summary>
public class Nop : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
    }
}

/// <summary>
/// přidá na vrchol zásobníku aktuální PC
/// </summary>
public class Pc : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        p.Push(p.Pc);
    }
}

/// <summary>
/// přidá konstantu na vrchol zásobníku
/// </summary>
public class Push : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        p.Push(param);
    }
}

/// <summary>
/// odebere vrchní hodnotu ze zásobníku
/// </summary>
public class Pop : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        p.Pop();
    }
}

/// <summary>
/// prohodí dvě nejvrchnější hodnoty na zásobníku
/// </summary>
public class Swap : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        uint a = p.Pop();
        uint b = p.Pop();
        p.Push(a);
        p.Push(b);
    }
}

/// <summary>
/// duplikuje vrchní hodnotu na zásobníku
/// </summary>
public class Dup : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        uint a = p.Pop();
        p.Push(a);
        p.Push(a);
    }
}

/// <summary>
/// přidá na vrchol zásobníku aktuální velikost (počet prvků) zásobníku
/// </summary>
public class PushSS : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        p.Push(Process.StackSize - p.Sp);
    }
}



// Instrukce pro přístup k hlavní paměti:

/// <summary>
/// - odebere jednu hodnotu ze zásobníku a použije ji jako index do paměti
/// - hodnotu načtenou z paměti přidá na vrchol zásobníku
/// </summary>
public class Load : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        uint address = p.Pop();
        uint value = mem.Read(p, address);
        p.Push(value);
    }
}

/// <summary>
/// - odebere ze zásobníku dvě hodnoty (nejdříve adresu na vrcholu zásobníku a pak hodnotu k zápisu)
/// - adresu použije jako index do paměti, kam zapíše hodnotu k zápisu
/// </summary>
public class Store : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        uint address = p.Pop();
        uint value = p.Pop();
        mem.Write(p, address, value);
    }
}


// Aritmetické operace:

/// <summary>
/// - odebere dvě hodnoty z vrcholu zásobníku
/// - jejich součet poté přidá na vrchol zásobníku
/// </summary>
public class Add : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        uint a = p.Pop();
        uint b = p.Pop();
        p.Push(a + b);
    }
}
/// <summary>
/// - odebere dvě hodnoty z vrcholu zásobníku
/// - jejich rozdíl (první − druhá) poté přidá na vrchol zásobníku
/// </summary>
public class Sub : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        uint a = p.Pop();
        uint b = p.Pop();
        p.Push(a - b);
    }
}

/// <summary>
/// - odebere dvě hodnoty z vrcholu zásobníku
/// - jejich podíl (první/druhá) poté přidá na vrchol zásobníku
/// - dělení nulou způsobuje ukončení procesu
/// </summary>
public class Div : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        uint a = p.Pop();
        uint b = p.Pop();

        if (b == 0) throw new DivisionByZero();

        p.Push(a / b);
    }
}

/// <summary>
/// - odebere dvě hodnoty z vrcholu zásobníku
/// - na vrchol zásobníku přidá první hodnotu umocněnou na druhou hodnotu
/// </summary>
public class Pow : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        uint a = p.Pop();
        uint b = p.Pop();

        //Console.WriteLine($"Pow({a},{b})");

        p.Push(Binpow(a, b));
    }

    public static uint Binpow(uint a, uint b)
    {
        uint res = 1;
        while (b > 0)
        {
            if ((b & 0b1) == 1)
            {
                res = res * a;
            }
            a = a * a;
            b >>= 1;
        }
        return res;
    }

    public static uint Power(uint a, uint b)
    {
        if (b == 0) return 1;
        if (b == 1) return 1;
        if (a == 1) return 1;

        if ((a % 10 == 0 || a % 2 == 0) && b > 32) return 0;

        uint result = a;
        for (int i = 1; i < b; i++)
        {
            result *= a;
        }
        return result;
    }
}


// Skokové instrukce:

/// <summary>
/// - odebere jednu hodnotu z vrcholu zásobníku
/// - pokud je hodnota rovna 0, skočí na adresu PC + immediate
/// </summary>
public class Brz : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        uint x = p.Pop();

        if (x == 0)
        {
            p.Pc += param;
        }

    }
}

/// <summary>
/// - odebere jednu hodnotu z vrcholu zásobníku
/// - pokud je hodnota rovna 3, skočí na adresu PC + immediate
/// </summary>
public class Br3 : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        uint x = p.Pop();

        if (x == 3)
        {
            p.Pc += param;
        }

    }
}

/// <summary>
/// - odebere jednu hodnotu z vrcholu zásobníku
/// - pokud je hodnota rovna 7, skočí na adresu PC + immediate
/// </summary>
public class Br7 : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        uint x = p.Pop();

        if (x == 7)
        {
            p.Pc += param;
        }

    }
}

/// <summary>
/// - odebere dvě hodnoty z vrcholu zásobníku
/// - pokud je první hodnota vetší nebo rovna druhé, skočí na adresu PC + immediate
/// </summary>
public class Brge : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        uint a = p.Pop();
        uint b = p.Pop();

        if (a >= b)
        {
            p.Pc += param;
        }

    }
}

/// <summary>
/// nepodmíněný skok na adresu immediate
/// </summary>
public class Jmp : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        p.Pc = param;

    }
}



// Speciální instrukce:

/// <summary>
/// ukončí proces
/// </summary>
public class ArmedBomb : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        p.Die("Armed bomb");
    }
}

/// <summary>
/// změní se na armed bomb po prvním vykonání instrukce
/// </summary>
public class Bomb : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        mem.Write(p, p.Pc, 0x12);

    }

}

/// <summary>
/// - počkej dokud další proces (nebo procesy) nedorazí na další instrukci tohoto typu
/// (kdekoliv v paměti)
/// - PC se po této instrukci neinkrementuje dokud nějaký jiný proces tuto instrukci nevykoná také
/// - když se najdou alespoň dva procesy čekající na této instrukci tak se jejich PC prohodí
/// a poté se jejich příslušné PC inkrementují již normálně a vliv instrukce tak končí
/// - procesy, které už skončily s chybou, se teleportace neúčastní (z pohledu teleportační
/// sémantiky jakoby neexistují).
/// - pokud je procesů více než dva, operace prohození je zobecněná na cyklické posunutí.
/// Pořadí procesů určuje jejich zápis na vstupu a posouvají se doprava. Jinými slovy,
/// jsou-li zadány tři procesy na vstupu a je-li v nějakém cyklu první zadaný proces na
/// adrese 1, druhý na adrese 8 a třetí na adrese 5, na každé z těchto adres je instrukce
/// teleport a všechny tři procesy jsou naživu, postupují do dalšího cyklu s následujícími
/// hodnotami PC: první proces 9, druhý 6 a třetí 2.
/// </summary>
public class TlPort : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        p.Teleport = true;
    }
}

/// <summary>
/// - ve vzdálenosti 2i, i ∈ {1, 2, 3} v obou směrech od PC uloží do hlavní paměti číslo
/// odpovídající instrukci bomb s nulovým argumentem
/// </summary>
public class Jntar : Instruction
{
    public override void Execute(Process p, Memory mem, uint param)
    {
        for (uint i = 1; i <= 3; i++)
        {
            mem.Write(p, (uint)p.Pc+Pow.Binpow(2, i), 0x13);
            mem.Write(p, (uint)p.Pc-Pow.Binpow(2, i), 0x13);
        }
    }
}
