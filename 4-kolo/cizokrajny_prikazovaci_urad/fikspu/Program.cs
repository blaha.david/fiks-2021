﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

public static class Program
{
    public static bool DEBUG = false;
    public static void Main()
    {
        int Q = int.Parse(Console.ReadLine());
        for (int q = 0; q < Q; q++)
        {
            Solve();
        }
    }

    public static void Solve(bool pass = false)
    {
        int P = int.Parse(Console.ReadLine());

        Fikspu fikspu = new Fikspu(P);

        for (int p = 0; p < P; p++)
        {
            string[] line = Console.ReadLine().Split(' ');
            uint offset = uint.Parse(line[0]);
            int len = int.Parse(line[1]);

            uint[] program = new uint[len];
            for (int i = 0; i < len; i++)
            {
                program[i] = uint.Parse(line[i + 2]);
            }

            fikspu.SpawnProcess(p, offset, program);
        }

        if (pass) return;


        fikspu.Print();
        bool running = true;
        while (running && fikspu.ClockCycles < 5000)
        {
            //Console.ReadKey();
            Thread.Sleep(700);
            running = fikspu.Tick();
            fikspu.Print();
        }
        fikspu.Print();

        Console.WriteLine($"{fikspu.Get42()} {fikspu.SumPCs()}");
    }
}


class Fikspu
{
    private Memory mem;
    private Process[] processes;

    public int ClockCycles {get; private set;} = 0;

    public Fikspu(int processCount)
    {
        processes = new Process[processCount];
        mem = new Memory();
    }

    public void SpawnProcess(int index, uint offset, uint[] program)
    {
        processes[index] = new Process(index, offset);
        for (int i = 0; i < program.Length; i++)
        {
            mem.Set((long)offset + i, program[i]);
        }
    }

    public bool Tick()
    {
        ClockCycles++;

        bool alive = false; // is at least one process alive

        List<Process> waitTp = new List<Process>();

        foreach (Process p in processes)
        {
            if (p.Dead) continue;
            alive = true;

            if (p.Teleport)
            {
                waitTp.Add(p);
                continue;
            }

            try
            {
                uint rawInstruction = mem.Read(p, p.Pc);

                uint instructionId = rawInstruction & 0xff;
                uint param = rawInstruction >> 8 & 0xffff;


                Instruction instruction = Instruction.DecodeInstruction(instructionId);
                //Console.WriteLine("Raw: 0b" + Convert.ToString(rawInstruction, 2).PadLeft(32, '0'));
                //Console.WriteLine($"Process {p.Index} executing '{instruction.ToString()}' with param '{param}'");

                // Enable DEBUG
                //Console.WriteLine($"Cycle: {ClockCycles,4},   [42]: {mem.Get(42)},   PC: {p.Pc,3},   Ins: {instruction,6},   Param: {param,5},   Stack: [{string.Join(',', p.Stack)}]");

                instruction.Execute(p, mem, param);

                if (p.Teleport)
                {
                    waitTp.Add(p);
                }
                else if (!p.Dead)
                {
                    p.Pc = (p.Pc+1) % 256;
                }
            }
            catch (ProcessException e)
            {
                Kill(p, e.GetType().ToString());
            }
        }


        if (waitTp.Count > 1)
        {
            uint tmp = waitTp[0].Pc;
            for (int i = 0; i < waitTp.Count-1; i++)
            {
                waitTp[i].Pc = waitTp[(i+1)%waitTp.Count].Pc;
            }
            waitTp[waitTp.Count-1].Pc = tmp;

            for (int i = 0; i < waitTp.Count; i++)
            {
                waitTp[i].Pc = (waitTp[i].Pc + 1) % 256;
                waitTp[i].Teleport = false;
            }

        }

        return alive;
    }


    private void Kill(Process p, string reason)
    {
        p.Die(reason);
    }

    public void Print()
    {
        Console.Clear();
        //Console.WriteLine("\n");

        int w = Console.WindowWidth;
        int h = Console.WindowHeight;
        int columnWidth = (w - 2) / processes.Length;

        // Top row
        StringBuilder rowBuilder = new StringBuilder();
        rowBuilder.Append("+");
        for (int i = 0; i < processes.Length; i++)
        {
            rowBuilder.Append(new string('-', columnWidth - 1));
            rowBuilder.Append("+");
        }

        string row = rowBuilder.ToString();

        Console.WriteLine($"Clock cycles: {ClockCycles}");
        Console.WriteLine($"Value at [42]: {mem.Get(42)}, sum of PCs: {SumPCs()}");


        // Names
        Console.WriteLine(row);
        Console.Write("|");
        for (int i = 0; i < processes.Length; i++)
        {
            if (processes[i].Dead) Console.ForegroundColor = ConsoleColor.Red;
            else Console.ForegroundColor = ConsoleColor.White;

            string status = processes[i].Dead ? processes[i].Reason : "Running";

            Console.Write(AlignCenter(processes[i].Index.ToString() + $" ({status})", columnWidth - 1));
            Console.Write("|");
        }

        Console.WriteLine();

        Console.WriteLine(row);

        // Program
        for (uint n = 0; n < 10; n++)
        {
            Console.Write("|");
            for (int i = 0; i < processes.Length; i++)
            {
                if (processes[i].Dead) Console.ForegroundColor = ConsoleColor.Red;
                else Console.ForegroundColor = ConsoleColor.White;

                uint inst = mem.Get(processes[i].Pc + n);

                uint instId = inst & 0xff;
                uint param = inst >> 8 & 0xffff;
                if (Instruction.IsValid(instId))
                {
                    string addr = $"{processes[i].Pc+n}: ";
                    Console.Write(addr + AlignCenter($"{Instruction.DecodeInstruction(instId).ToString()} {param}", columnWidth - addr.Length - 1));
                }
                else
                {
                    Console.Write(AlignCenter(inst.ToString(), columnWidth - 1));
                }

                Console.Write("|");
            }

            Console.WriteLine();
        }

        //// If died, print reason
        //Console.Write("|");
        //for (int i = 0; i < processes.Length; i++)
        //{
            //if (processes[i].Dead)
            //{
                //Console.ForegroundColor = ConsoleColor.Red;
                //Console.Write(AlignCenter(processes[i].Reason, columnWidth - 1));
                //Console.Write("|");
            //}
            //else
            //{
                //Console.ForegroundColor = ConsoleColor.White;

                //Console.Write(new string(' ', columnWidth - 1));
                //Console.Write("|");
            //}
        //}

        //Console.WriteLine();

        Console.WriteLine(row);

        // Stack
        for (int n = 0; n < Process.StackSize; n++)
        {
            Console.Write("|");
            for (int i = 0; i < processes.Length; i++)
            {
                if (n < processes[i].Sp)
                {
                    Console.Write(new string(' ', columnWidth - 1));
                    Console.Write("|");
                    continue;
                }

                if (processes[i].Dead) Console.ForegroundColor = ConsoleColor.Red;
                else Console.ForegroundColor = ConsoleColor.White;

                uint value = processes[i].Stack[n];
                Console.Write(AlignCenter(value.ToString(), columnWidth - 1));
                Console.Write("|");
            }

            Console.WriteLine();
        }

        Console.WriteLine(row);
    }

    public uint Get42()
    {
        return mem.Get(42);
    }

    public long SumPCs()
    {
        long sum = 0;
        foreach (Process p in processes)
        {
            sum += (long)p.Pc % Memory.MEMORY_SIZE;
        }
        return sum;
    }

    private static string AlignCenter(string text, int width)
    {
        text = text.Length > width ? text.Substring(0, width - 3) + "..." : text;

        if (string.IsNullOrEmpty(text))
        {
            return new string(' ', width);
        }
        else
        {
            return text.PadRight(width - (width - text.Length) / 2).PadLeft(width);
        }
    }
}
