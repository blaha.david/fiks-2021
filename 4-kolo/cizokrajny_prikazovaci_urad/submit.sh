#!/bin/bash

COOKIE='bi7tgl4n7obv8fa1hnlpgktei1'

# generate
curl 'https://fiks.fit.cvut.cz/sfinx/odevzdavatko/146/vygenerovat' -H "Cookie: PHPSESSID=$COOKIE" -L

# download
curl 'https://fiks.fit.cvut.cz/sfinx/odevzdavatko/9429/stahnoutVstup' -H 'Accept: */*' -H "Cookie: PHPSESSID=$COOKIE" -o 'input.txt' -L

# execute
fikspu/bin/Release/net6.0/linux-x64/fikspu < input.txt > output.txt 2> err.log

# upload
curl 'https://fiks.fit.cvut.cz/sfinx/odevzdavatko/146' -F 'result=@output.txt' -F '_submit=Odevzdat' -F '_do=submitForm-submit' -H "Cookie: PHPSESSID=$COOKIE" -L
