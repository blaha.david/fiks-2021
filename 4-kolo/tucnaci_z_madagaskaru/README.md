
# Tučňáci z Madagaskaru

## Popis algoritmu

zprávy budou zašifrované XORem - klíč si předem vymění,
výsledek se otočí rot8000,
poté bajty budou převedeny do base85,
to bude opět zašifrované XORem s druhým klíčem, který si předem dohodnou.
to bude převedeno do base62,
to bude zašifrované pomocí Vigenerovi šifry - opět klíčem, který si před začátkem vymění,
pak se na to použije klasický rot13
a to se odešle.

Kombinace všech těchto různých šifer zajituje, že nikdo nebude schopný odposlechnutou konverzaci rozšifrovat a přečíst a proto je naprosto bezpečná.


## Důkaz správnosti

všechny tyto šifry už existují dlouho a nikdo si zatím nestěžoval, že by nefungovali správně, takže jsou správné.
