
# FIKS 2021

## 4. kolo

### [Tučňáci z Madagaskaru](tucnaci_z_madagaskaru/) `0/10`
Zamysli se!
[download pdf](https://fiks.fit.cvut.cz/files/tasks/season8/round4/sifrovani.pdf)


### [Cizokrajný přikazovací úřad](cizokrajny_prikazovaci_urad/) `10/10`
Odpověz Sfinze!
[download pdf](https://fiks.fit.cvut.cz/files/tasks/season8/round4/cizokrajny_prikazovaci_urad.pdf)


### [Krmení](krmeni/) `10/10`
Odpověz Sfinze!
[download pdf](https://fiks.fit.cvut.cz/files/tasks/season8/round4/krmeni.pdf)


