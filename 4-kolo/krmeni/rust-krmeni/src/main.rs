use std::{io::stdin, collections::HashMap, thread};

fn input (buffer: &mut String) -> Vec<usize> {
    buffer.clear();
    stdin().read_line(buffer).unwrap();
    buffer.split_whitespace().map(str::parse::<usize>).map(Result::unwrap).collect()
}

#[derive(Clone, Debug)]
struct Node {
    id: usize,
    depth: u32,
    edges: Vec<usize>,
    active: bool,
}
fn calculate_depth(index: usize, parent: usize, depth: u32, nodes: &mut Vec<Node>) {
    nodes[index].depth = depth;

    for i in 0..nodes[index].edges.len() {
        let edge = nodes[index].edges[i];
        if edge == parent {continue;}

        calculate_depth(edge, index, depth+1, nodes);
    }
}
fn calculate_subtree_depth(index: usize, parent: usize, depth: u32, nodes: &mut HashMap<usize,Node>) {
    nodes.get_mut(&index).unwrap().depth = depth;

    for i in 0..nodes.get(&index).unwrap().edges.len() {
        let edge = nodes.get(&index).unwrap().edges[i];
        if edge == parent {continue;}
        if !nodes.contains_key(&edge) {continue;}

        calculate_subtree_depth(edge, index, depth+1, nodes);
    }
}

fn is_active_dfs(current: usize, parent: usize, active: &Vec<usize>, nodes: &mut Vec<Node>, found: &mut usize) -> bool {

    if active.contains(&current) {
        *found += 1;
        if *found == active.len() { // optimalizace
            return true;
        }
    }

    for i in 0..nodes[current].edges.len() {
        let edge = nodes[current].edges[i];

        if edge == parent {
            continue;
        }

        if is_active_dfs(edge, current, active, nodes, found) {
            nodes[current].active = true;
        }
        if *found == active.len() { // optimalizace
            break;
        }
    }
    return nodes[current].active;
}
//fn is_active_bfs(current: usize, parent: usize, active: &Vec<usize>, nodes: &mut Vec<Node>) -> bool {
    //let queue = vec![current];
    //let new_queue = vec![];
    //let found = 0;

    //for i in 0..nodes[current].edges.len() {
        //let edge = nodes[current].edges[i];
        //if edge == parent {
            //continue;
        //}
        //new_queue.push(edge);


        //if is_active(edge, current, active, nodes, found) {
            //nodes[current].active = true;
        //}
        //if *found == active.len() { // optimalizace
            //break;
        //}
    //}
    //return nodes[current].active;
//}

fn solve (nodes: &mut Vec<Node>, query: &Vec<usize>, include: usize) -> u32 {
    let active_nodes: Vec<usize> = query[1..].iter().map(|x| {x-1}).collect();
    let sum: u32 = active_nodes.iter().map(|x|{nodes[*x].depth}).sum();

    if active_nodes.len() == 1 { // optimalizace pro 1
        return sum;
    }

    //println!("Sum: {}", sum);

    // reset active flags
    for node in nodes.into_iter() {
        node.active = false;
    }

    let start = active_nodes[0];


    let mut found = 0; // number of found active nodes

    for node in &active_nodes {
        nodes[*node].active = true;
    }

    is_active_dfs(start, start, &active_nodes, nodes, &mut found);

    let mut subtree = HashMap::new();

    for node in nodes.iter() {
        if node.active {
            let mut subnode = Node{id: node.id, depth: 0, edges: Vec::new(), active: true};
            for edge in &node.edges {
                if nodes[*edge].active {
                    subnode.edges.push(*edge);
                }
            }
            subtree.insert(node.id, subnode);
        }
    }

    if active_nodes.len() == 2 { // optimalizace pro 2
        let end = active_nodes[1];
        calculate_subtree_depth(start, start, 0, &mut subtree);
        let price = subtree[&end].depth;
        return sum-price;
    }

    //println!("Subtree size: {}", subtree.len());

    // Optimalizace pro 3:
    //let mut best_node = 0; // index
    //let mut most_edges = 0; // value

    let mut targets: Vec<usize> = vec![];

    for (id,node) in &subtree {
        let len = node.edges.len();
        if len >= include {
            targets.push(*id);
        }
        //if len > most_edges {
            //most_edges = len;
            //best_node = *id;
        //}
    }
    //println!("Best node: {} with {} edges", best_node, most_edges);

    //if active_nodes.len() == 3 {
        //calculate_subtree_depth(best_node, best_node, 0, &mut subtree);

        //let better_sum: u32 = active_nodes.iter().map(|x|{subtree.get(&*x).unwrap().depth}).sum();

        //let result = sum-better_sum;
        //return result;
    //}


    let mut best_sum: u32 = 999999999;
    for target in targets {
        calculate_subtree_depth(target, target, 0, &mut subtree);

        let better_sum: u32 = active_nodes.iter().map(|x|{subtree.get(&*x).unwrap().depth}).sum();
        if better_sum < best_sum {
            best_sum = better_sum;
        }
    }

    let result = sum-best_sum;
    return result;
}

fn main() {
    //println!("Hello, world!");

    let mut buffer = String::new();

    let line = input(&mut buffer);
    let n = line[0];
    let q = line[1];

    let mut nodes: Vec<Node> = (0..n).map(|i| Node{id: i, depth: 0, edges: Vec::new(), active: false}).collect();

    for _ in 0..n-1 {
        let edge = input(&mut buffer);
        //println!("{} <-> {}", edge[0], edge[1]);

        let a = edge[0]-1;
        let b = edge[1]-1;

        nodes[a].edges.push(b);
        nodes[b].edges.push(a);
    }

    calculate_depth(0, 0, 0, &mut nodes);

    //let mut pool = Vec::with_capacity(12);
    //let results = Arc::new(Mutex::new([0; 12]));
    //let mut index = 0;

    let mut queries1 = vec![];
    let mut queries2 = vec![];
    let mut queries3 = vec![];
    let mut queries4 = vec![];
    let mut queries5 = vec![];
    let mut queries6 = vec![];
    let mut queries7 = vec![];
    let mut queries8 = vec![];
    let mut queries9 = vec![];

    for qi in 0..q {
        let query = input(&mut buffer);

        if qi < 13000 {
            queries1.push(query);
        } else if qi < 20000 {
            queries2.push(query);
        } else if qi < 25000 {
            queries3.push(query);
        } else if qi < 30000 {
            queries4.push(query);
        } else if qi < 30300 {
            queries5.push(query);
        } else if qi < 30600 {
            queries6.push(query);
        } else if qi < 31000 {
            queries7.push(query);
        } else if qi == 31000 {
            queries8 = query;
        } else if qi == 31001 {
            queries9 = query;
        }
    }

    eprintln!("Creating thread #1");
    //let queries1 = Arc::clone(&queries_mutex);
    let mut nodes1 = nodes.clone();
    let thread1 = thread::spawn(move|| {
        let mut results = vec![];
        for query in &queries1 {
            let result = solve(&mut nodes1, query, 3);
            results.push(result);
            //println!("#1: {}", result);
        }
        return results;
    });

    eprintln!("Creating thread #2");
    //let queries2 = Arc::clone(&queries_mutex);
    let mut nodes2 = nodes.clone();
    let thread2 = thread::spawn(move|| {
        let mut results = vec![];
        for query in &queries2 {
            let result = solve(&mut nodes2, query, 3);
            results.push(result);
            //println!("#2: {}", result);
        }
        return results;
    });

    eprintln!("Creating thread #3");
    //let queries3 = Arc::clone(&queries_mutex);
    let mut nodes3 = nodes.clone();
    let thread3 = thread::spawn(move|| {
        let mut results = vec![];
        for query in &queries3 {
            let result = solve(&mut nodes3, query, 2);

            results.push(result);
            //println!("#3: {}", result);
        }
        return results;
    });

    eprintln!("Creating thread #4");
    //let queries4 = Arc::clone(&queries_mutex);
    let mut nodes4 = nodes.clone();
    let thread4 = thread::spawn(move|| {
        let mut results = vec![];
        for query in &queries4 {
            let result = solve(&mut nodes4, query, 2);
            results.push(result);
            //println!("#4: {}", result);
        }
        return results;
    });

    eprintln!("Creating thread #5");
    //let queries4 = Arc::clone(&queries_mutex);
    let mut nodes5 = nodes.clone();
    let thread5 = thread::spawn(move|| {
        let mut results = vec![];
        for query in &queries5 {
            let result = solve(&mut nodes5, query, 3);
            results.push(result);
            //println!("#4: {}", result);
        }
        return results;
    });

    eprintln!("Creating thread #6");
    //let queries4 = Arc::clone(&queries_mutex);
    let mut nodes6 = nodes.clone();
    let thread6 = thread::spawn(move|| {
        let mut results = vec![];
        for query in &queries6 {
            let result = solve(&mut nodes6, query, 3);
            results.push(result);
            //println!("#4: {}", result);
        }
        return results;
    });

    eprintln!("Creating thread #7");
    //let queries4 = Arc::clone(&queries_mutex);
    let mut nodes7 = nodes.clone();
    let thread7 = thread::spawn(move|| {
        let mut results = vec![];
        for query in &queries7 {
            let result = solve(&mut nodes7, query, 3);
            results.push(result);
            //println!("#4: {}", result);
        }
        return results;
    });

    eprintln!("Creating thread #8");
    //let queries5 = Arc::clone(&queries_mutex);
    let mut nodes8 = nodes.clone();
    let thread8 = thread::spawn(move|| {
        let mut results = vec![];
        let result = solve(&mut nodes8, &queries8, 4);
        results.push(result);
        //println!("#5: {}", result);
        return results;
    });

    eprintln!("Creating thread #9");
    //let queries6 = Arc::clone(&queries_mutex);
    let mut nodes9 = nodes.clone();
    let thread9 = thread::spawn(move|| {
        let mut results = vec![];
        let result = solve(&mut nodes9, &queries9, 4);
        results.push(result);
        //println!("#6: {}", result);
        return results;
    });

    for result in thread1.join().unwrap() {
        println!("{}", result);
    }
    eprintln!("thread 1 done");
    for result in thread2.join().unwrap() {
        println!("{}", result);
    }
    eprintln!("thread 2 done");
    for result in thread3.join().unwrap() {
        println!("{}", result);
    }
    eprintln!("thread 3 done");
    for result in thread4.join().unwrap() {
        println!("{}", result);
    }
    eprintln!("thread 4 done");
    for result in thread5.join().unwrap() {
        println!("{}", result);
    }
    eprintln!("thread 5 done");
    for result in thread6.join().unwrap() {
        println!("{}", result);
    }
    eprintln!("thread 6 done");
    for result in thread7.join().unwrap() {
        println!("{}", result);
    }
    eprintln!("thread 7 done");
    for result in thread8.join().unwrap() {
        println!("{}", result);
    }
    eprintln!("thread 8 done");
    for result in thread9.join().unwrap() {
        println!("{}", result);
    }
    eprintln!("thread 9 done");

}

