#! /usr/bin/env python3
t = int(input())
for _ in range(t):
    abc = input().split(" ")
    aktualneNejvyssiCislo = -1
    pocetNejvyssichCisel = 0
    for cislo in abc:
        cislo = int(cislo)
        if cislo >= aktualneNejvyssiCislo:
            aktualneNejvyssiCislo = cislo
    for cislo in abc:
        if int(cislo) == aktualneNejvyssiCislo:
            pocetNejvyssichCisel += 1
    printString = ""
    for cislo in abc:
        if pocetNejvyssichCisel > 1:
            if int(cislo) >= aktualneNejvyssiCislo:
                printString += "1 "
            else:
                printString += str(aktualneNejvyssiCislo - int(cislo) + 1) + " "
        else:
            if int(cislo) >= aktualneNejvyssiCislo:
                printString += "0 "
            else:
                printString += str(aktualneNejvyssiCislo - int(cislo) + 1) + " "
    print(printString[:-1])
