#! /usr/bin/env python3

def solve(arr: list):
    min_diff = 999999999
    for i in range(len(arr)):
        for j in range(i, len(arr)):
            diff = abs(arr[i] - arr[j])
            if diff < min_diff and diff > 0:
                min_diff = diff

    if min_diff == 999999999:
        return -1

    return min_diff



if __name__ == "__main__":
    t = int(input())
    for tt in range(t):
        # print(f'\ninput #{tt}')
        n = int(input())
        arr = [int(i) for i in input().split(' ')]

        print(solve(arr))
