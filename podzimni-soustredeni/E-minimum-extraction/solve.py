#! /usr/bin/env python3

def solve(arr: list):
    prev_min = -9999999999
    minimum = min(arr)

    best = minimum

    while True:
        # print(f'{prev_min=}, {minimum=}, {arr=}')

        arr.remove(minimum)

        for i,el in enumerate(arr):
            arr[i] = el - minimum

        if len(arr) == 1:
            if arr[0] > best:
                best = arr[0]
            break

        # print(arr)

        prev_min = minimum
        minimum = min(arr)
        if minimum > prev_min:
            best = minimum

    return best




if __name__ == "__main__":
    t = int(input())
    for tt in range(t):
        # print(f'\ninput #{tt}')
        n = int(input())
        arr = [int(i) for i in input().split(' ')]
        if len(arr) == 1:
            print(arr[0])
            continue

        print(solve(arr))


