﻿using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        int t = int.Parse(Console.ReadLine());
        for (int i = 0; i < t;)
        {
            //Console.WriteLine($"vstup {i}");
            int n = int.Parse(Console.ReadLine());

            HashSet<int> currentset = new HashSet<int>();
            HashSet<int> newset = new HashSet<int>();
            currentset.Add(n);

            int depth = 0;

            while (currentset.Count > 0)
            {
                foreach (int number in currentset)
                {
                    if (number % 25 == 0)
                    {
                        Console.WriteLine(depth);
                        goto loopend;
                    }
                    string digits = number.ToString();

                    //Console.WriteLine($"digits: {digits}");

                    if (digits.Length == 1)
                    {
                        currentset.Remove(number);
                        continue;
                    }

                    for (int j = 0; j < digits.Length; j++)
                    {
                        string copy = new string(digits);
                        //Console.WriteLine($"copy: {copy}, j={j}");
                        copy = copy.Remove(j, 1);
                        newset.Add(int.Parse(copy));
                    }
                }


                HashSet<int> temp = currentset;
                currentset = newset;
                newset = temp;
                newset.Clear();
                depth++;
            }
loopend:
            i++;
        }
    }
}
