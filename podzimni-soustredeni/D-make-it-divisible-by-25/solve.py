#! /usr/bin/env python3

from copy import deepcopy

def solve(n:int):
    currentset = {n}

    newset = []

    depth = 0
    while len(currentset) != 0:
        for value in currentset:
            if value % 25 == 0:
                return depth

            digits = list(str(value))

            if len(digits) == 1:
                currentset.remove(value)
                continue

            for j,digit in enumerate(digits):
                copydigits = deepcopy(digits)
                copydigits.pop(j)
                # print(f'copy: {copydigits}')
                newset.append(int(''.join(copydigits)))

        # print(f'current: {currentset}, new: {newset}')
        currentset = list(dict.fromkeys(currentset))
        currentset = newset

        newset = []
        depth += 1

    print('nosolution?')


if __name__ == "__main__":
    t = int(input())
    for i in range(t):
        # print(f'test: {i}')
        n = int(input())
        print(solve(n))

