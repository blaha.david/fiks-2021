#! /usr/bin/env python
t = int(input())
for _ in range(t):
    keyboard = input()
    s = input()
    celek = 0
    indexMinulehoPismena = -1
    for pismeno in s:
        indexPismena = keyboard.index(pismeno)
        if not (indexMinulehoPismena == indexPismena or indexMinulehoPismena == -1):
            celek = celek + abs(indexMinulehoPismena - indexPismena)
        indexMinulehoPismena = indexPismena
    print(celek)

