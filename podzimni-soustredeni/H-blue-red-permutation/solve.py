#! /usr/bin/env python3

RED = True
BLUE = False

def solve(arr: list[int], colors: list[bool]):
    current = 0 # zaciname na jednicce

    #loop:

    # najdi dalsi
    next = current + 1
    if next in arr:

        # TODO: preferuj zmensujici se
        current = next
        index = arr.index(next)
        arr.pop(index)
        colors.pop(index)
        #continue
    else:
        pass
        # najdi nejblizsi






if __name__ == "__main__":
    t = int(input())
    for tt in range(t):
        print(f'\ninput #{tt}')
        n = int(input())
        arr = [int(i) for i in input().split(' ')]
        colors = [ RED if el == 'R' else BLUE for el in input() ]

        print(solve(arr, colors))
