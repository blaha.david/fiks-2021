#! /usr/bin/env python3
t = int(input())

def jeSude(cislo):
    if cislo % 2 == 0:
        return True
    return False

for _ in range(t):
    radek = input().split(" ")
    x0 = int(radek[0]) # zacatecni pozice
    n = int(radek[1]) # pocet kroku
    vysledek = 0
    zbytekPoDeleniPoctuKroku = n%4
    celociselneDeleniPoctuKroku = n//4
    if jeSude(x0):
        if zbytekPoDeleniPoctuKroku == 0:
            vysledek = x0
        elif zbytekPoDeleniPoctuKroku == 1:
            vysledek = x0 - (celociselneDeleniPoctuKroku * 4) - 1
        elif zbytekPoDeleniPoctuKroku == 2:
            vysledek = x0 + 1
        else:
            vysledek = x0 + (celociselneDeleniPoctuKroku + 1) * 4
    else:
        if zbytekPoDeleniPoctuKroku == 0:
            vysledek = x0
        elif zbytekPoDeleniPoctuKroku == 1:
            vysledek = x0 + celociselneDeleniPoctuKroku * 4 + 1
        elif zbytekPoDeleniPoctuKroku == 2:
            vysledek = x0 - 1
        else:
            vysledek = x0 - celociselneDeleniPoctuKroku - 1 * 4
    print(vysledek)
